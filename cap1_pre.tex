\chapter{Poliedros Regulares en el Espacio Euclideano $\bE^3$} \label{cap:PREE}

Los poliedros regulares son objetos que se han estudiado durante muchos siglos. Con el paso del tiempo se han aceptado diferentes definiciones de poliedro e incluso de regularidad.\\

Una definición puramente geométrica de poliedro es como una familia de polígonos pegados por aristas de manera convexa y podemos pensar que es regular si todos los polígonos son regulares, congruentes y se pegan \textit{igual por todos lados}. Para los fines de este trabajo estos conceptos no son muy útiles pues resultan un poco restrictivos pues, por ejemplo, se conoce desde hace mucho que con estas características hay únicamente 5 poliedros regulares.\\

En la primera sección de este capítulo se darán las definiciones básicas de lo que entenderemos \textit{poliedro regular}, mencionaremos algunas propiedades generales y se presentarán algunos ejemplos. En la segunda sección se hará énfasis en los poliedros regulares vistos como objetos en el espacio euclideano $\bE^3$ y se estudiarán con más detalle aquellos que usaremos en el trabajo. 

\section{Poliedros Regulares en Abstracto} \label{sec:PRA}

\begin{defn} \label{polabs}
Un \emph{poliedro} \index{Poliedro!abstracto} (en abstracto) $\P$ es un conjunto parcialmente ordenado con una \emph{función de rango} estrictamente creciente de imagen $\{-1,0,1,2,3 \}$. Llamarémos \emph{vértices}, \index{Vértice} \emph{aristas} \index{Arista} y \emph{caras} \index{Cara} a los elementos de rango $0$, $1$ y $2$ respectivamente. Una \emph{bandera} \index{Bandera} es un subconjunto totalmente ordenado maximal. Además $\P$ satisface las siguientes propiedades:
\begin{enumerate}
\item Existe un único elemento mínimal $F_{-1}$ y un único elemento maximal $F_3$
\item Las banderas tienen exactamente $5$ elementos.
\item $\P$ es \emph{fuertemente conexo por banderas}: \index{Bandera!Conexidad fuerte por} Dadas dos banderas $\Phi$ y $\Psi$ existe una sucesión de banderas $\Phi=\Phi_0, \Phi_1, \dots, \Phi_k=\Psi$ tales que para cualquier $i\in \{1,\dots,k\}$ $\Phi_{i-1}$ y $\Phi_i$ son \emph{adyacentes} \index{Bandera!adyacente} (difieren únicamente por un elemento) y además $\Phi \cap \Psi \subset \Phi_i$ para $i\in \{1,\dots,k\}$.
\item \label{diam} $\P$ satisface la \emph{propiedad del diamante}\index{Diamante, Propiedad del}: Para cada $j \in \{0,1,2\}$, dado $F$ un elemento de rango $j-1$, y $G$ un elemento de rango $j+1$ se tiene que $ \vert \{ H \in \P : F<H<G \} \vert = 2$.
\end{enumerate}
\end{defn}

\tcb{Aquí pondría un dibujo de, por ejemplo, un tetraedro (porque es fácil) visto como orden parcial}

Estas propiedades nos dicen que los poliedros a considerar satisfacen algunas condiciones combinatorias a las que estamos acostumbrados en los poliedros usuales (en un sentido geométrico). Por ejemplo, gracias a la propiedad \ref{diam} cada arista tiene exactamente dos vértices y está en exactamente dos caras.\\

La definición \ref{polabs} se puede generalizar considerando que la función de rango toma valores en $\{-1,0,1,\dots,n\}$ para $n \in \bN$. En este caso $\P$ es un \emph{politopo de rango $n$}\index{Politopo} o simplemente un $n$-politopo, las banderas \index{Bandera} tienen $n+2$ elementos y la propiedad del diamante\index{Diamante, Propiedad del} se satisface para cada $j \in \{0,1, \dots, n-1\}$. En este sentido, un poligono es un $2$-politopo y un poliedro es un $3$-politopo.\\

\tcb{posiblemente aquí pondría un dibujo de un pentagono o algo así como orden parcial, o quizá en el mismo dibujo de arriba}

Observemos también que el orden parcial nos define de manera natural una relación de incidencia, así que podemos pensar las aristas como parejas de vértices y las caras como familias de aristas, así cobran sentido frases como \textit{``un vértice que está en una arista''} o \textit{``una arista en una cara''} e incluso \textit{``un vértice en una cara''}. Más precisamente, cuando no haya confusión identificaremos a una cara $F$ con un la sección $$ F/F_{-1} := \{ H \in \P : H \leq F \} $$.

\tcb{aqui el dibujo del tetraedro mostrando cómo es el copo de una cara o tal vez más abajo para también ejemplificar cuál es la figura verticial}

\begin{defn}\label{figvert}
Dado un vértice $F_0$ de un poliedro $\P$ definimos la \emph{figura verticial de $F_0$} $F_3/F_0$ como el polígono (abstracto) dado por $$ F_3/F_0 := \{ H \in \P : F_0 \leq H \} $$
\end{defn}


Denotaremos por $\cF(\P)$ al conjunto de todas las banderas de $\P$. Si $\Phi$ es una bandera y $0 \leq i \leq 2$ denotaremos por $\Phi^i$ la única bandera \emph{$i$-adyacente} a $\Phi$, es decir, la única bandera que difiere de $\Phi$ únicamente en la cara de rango $i$. Definimos también de manera recursiva $\Phi^{i_{1} i_{2} \dots i_{k}}:=(\Phi^{i_{1} i_{2} \dots i_{k-1}})^{i_k}$ para $k \geq 2$. Tenemos así que se satisfacen \[ \Phi^{ii}=\Phi \text{ y } \Phi^{ij}=\Phi^{ji} \text{ si } |i-j|\geq 2\]

\tcb{no sé si aquí deba poner un dibujo de una bandera en el copo o algo así}

\begin{defn}
Si $\P$ y $\Q$ son poliedros $\phi: \P \rightarrow \Q$ es un \emph{isomorfismo de poliedros}\index{Isomorfismo} si $\phi$ es una biyección y tanto $\phi$ como $\phi^{-1}$ preservan orden, es decir, $F \leq G$ en $\P$ si y sólo si $\phi(F) \leq \phi(G)$ en $\Q$. Un \emph{automorfismo de $\P$}\index{Automorfismo} es un isomorfismo de $\P$ en sí mismo.  
\end{defn}

Denotaremos por $\G(\P)$ el conjunto de todos los automorfismos\index{Automorfismo!grupo de} de $\P$. Es claro que $\G(\P)$ es un grupo bajo la composición. Además existe una acción natural de $\G(\P)$ en $\cF(\P)$.

\begin{defn}
Diremos que un poliedro $\P$ es \emph{regular} si $\G(\P)$ actúa transitivamente en $\cF(\P)$, el conjunto de banderas de $\P$.
\end{defn}

Observemos que esta definición de regularidad mantiene la idea de regularidad geométrica pues, dadas dos caras (aristas o vértices) $F$ y $G$ de un poliedro $\P$ existe un automorfismo $\phi$ tal que $\phi (F)=G$. En particular cualesquiera dos caras son isomorfas y dos vértices tienen figuras verticiales isomorfas. 



