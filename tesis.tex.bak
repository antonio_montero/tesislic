\documentclass[12pt, twoside, openright]{book} 
\usepackage[utf8]{inputenc} %para poner acentos y caracteres de ese estilo
\usepackage[spanish, mexico]{babel}
\usepackage{amsmath, amsthm, amssymb, amsfonts}
\usepackage{graphicx, color} %gráficos usuales
%\usepackage{pstricks} %gráficos pstricks
%\usepackage{multido, pst-3dplot} %herramientas de pstricks
\usepackage{fancyhdr} %paquete para páginas fresas
\usepackage{caption, subcaption} %imágenes y subimágenes
\usepackage{enumerate} %personalización de listas
\usepackage{mathrsfs} %fuente scr
\usepackage[spanish]{babelbib}%para que la bibliografía esté en español
\usepackage{makeidx} %indice alfabético
\usepackage[section]{placeins} %para borrar el índice de flotantes 
%\usepackage{afterpage}
\usepackage[all]{xy} %diagramas "algebrosos"
\usepackage{appendix}

\unaccentedoperators


%%Páginas fresas: 
\pagestyle{fancy}
% con esto nos aseguramos de que las cabeceras
% de capítulo y de sección vayan en minúsculas
\renewcommand{\chaptermark}[1]{%
\markboth{#1}{}}
\renewcommand{\sectionmark}[1]{%
\markright{\thesection\ #1}}
\fancyhf{} % borra cabecera y pie actuales
\fancyhead[LE,RO]{\bfseries\thepage}
\fancyhead[LO]{\bfseries\rightmark}
\fancyhead[RE]{\bfseries\leftmark}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0pt}
\addtolength{\headheight}{0.5pt} % espacio para la raya
\fancypagestyle{plain}{%
\fancyhead{} % elimina cabeceras en páginas "plain"
\renewcommand{\headrulewidth}{0pt} % así como la raya
}

%%Las páginas en blanco son de verdad en blanco
\makeatletter
\def\cleardoublepage{\clearpage\if@twoside \ifodd\c@page\else
\hbox{}
\vspace*{\fill}
\begin{center}
%% aqui puede ir algo como "página intencionalmente en blanco"
\end{center}
\vspace{\fill}
\thispagestyle{empty}
\newpage
\if@twocolumn\hbox{}\newpage\fi\fi\fi}
\makeatother

%

%%colores
\definecolor{MyBlack}{RGB}{0,0,0}
\definecolor{MyRed}{RGB}{255,0,0}
\definecolor{MyBlue}{RGB}{0,0,255}
\definecolor{MyGreen}{RGB}{0,200,0}

%%comandos

\def\tcr#1{\textcolor{MyRed}{#1}}
\def\tcb#1{\textcolor{MyBlue}{#1}}
\def\tcbck#1{\textcolor{MyBlack}{#1}}
\def\tcg#1{\textcolor{MyGreen}{#1}}
\def\V#1{\vec{#1}}

\renewcommand{\leq}{\leqslant} %menores y mayores iguales decentes
\renewcommand{\geq}{\geqslant}
\renewcommand{\epsilon}{\varepsilon} %epsilon decente
\renewcommand{\subset}{\subseteq} %subseteq decente
\renewcommand{\qedsymbol}{$\blacksquare$} %cuadrito decente
%\def\proof{{\noindent\bf Proof.}\hskip 0.2truecm}
%\def\BBox{\kern  -0.2cm\hbox{\vrule width 0.2cm height 0.2cm}}
%\def\endproof{\hspace{.20in} \BBox\vspace{.20in}}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

%abreviaturas
\let\acenv\v %para cambiar el comando \v por \acenv
\def\G{{\Gamma}}
\def\P{{\mathcal{P}}}
\def\Q{{\mathcal{Q}}}
\def\Tt{{\mathbb{T}^{3}_{\tau}}}
\def\E{{\mathbb{E}^3}}
\def\TT{{\mathbb{T}^3}}
\def\T{{\tau}}
\def\e{{\mathrm{e}}}
\def\L{{\Lambda}}
\def\v{{\vec{\upsilon}}}
\def\w {{\vec{\omega}}}
\def\r{{\rho}}
\def\s{{\sigma}}
\def\tetra{{\mathfrak{T}}}
\def\Tetra{{\Gamma(\cT)}}
\def\Octa{{\Gamma(\cO)}}
\def\Ico{{\Gamma(\cI)}}
\def\cl{{\L_{(1,0,0)}}}
\def\fcl{{\L_{(1,1,0)}}}
\def\bcl{{\L_{(1,1,1)}}}
\def\PCa{{\cP \cC_1}}
\def\PCb{{\cP \cC_2}}
\def\PCc{{\cP \cC_3}}
\def\Id{{Id}}
\def\F{{\cF}}

%%%Letritas
\def\cA{{\mathcal{A}}} \def\cB{{\mathcal{B}}} \def\cC{{\mathcal{C}}} \def\cD{{\mathcal{D}}} \def\cE{{\mathcal{E}}} \def\cF{{\mathcal{F}}} \def\cG{{\mathcal{G}}} \def\cH{{\mathcal{H}}} \def\cI{{\mathcal{I}}} \def\cJ{{\mathcal{J}}} \def\cK{{\mathcal{K}}} \def\cL{{\mathcal{L}}} \def\cM{{\mathcal{M}}} \def\cN{{\mathcal{N}}} \def\cO{{\mathcal{O}}} \def\cP{{\mathcal{P}}} \def\cQ{{\mathcal{Q}}} \def\cR{{\mathcal{R}}} \def\cS{{\mathcal{S}}} \def\cT{{\mathcal{T}}} \def\cV{{\mathcal{V}}} \def\cU{{\mathcal{U}}} \def\cW{{\mathcal{W}}} \def\cX{{\mathcal{X}}} \def\cY{{\mathcal{Y}}} \def\cZ{{\mathcal{Z}}}

\def\bA{{\mathbb{A}}} \def\bB{{\mathbb{B}}} \def\bC{{\mathbb{C}}} \def\bD{{\mathbb{D}}} \def\bE{{\mathbb{E}}} \def\bF{{\mathbb{F}}} \def\bG{{\mathbb{G}}} \def\bH{{\mathbb{H}}} \def\bI{{\mathbb{I}}} \def\bJ{{\mathbb{J}}} \def\bK{{\mathbb{K}}} \def\bL{{\mathbb{L}}} \def\bM{{\mathbb{M}}} \def\bN{{\mathbb{N}}} \def\bO{{\mathbb{O}}} \def\bP{{\mathbb{P}}} \def\bQ{{\mathbb{Q}}} \def\bR{{\mathbb{R}}} \def\bS{{\mathbb{S}}} \def\bT{{\mathbb{T}}} \def\bU{{\mathbb{U}}} \def\bV{{\mathbb{V}}} \def\bW{{\mathbb{W}}} \def\bX{{\mathbb{X}}} \def\bY{{\mathbb{Y}}} \def\bZ{{\mathbb{Z}}}

%Habilitar únicamente si el está disponible el paquete mathrsfs
\def\sA{{\mathscr{A}}} \def\sH{{\mathscr{H}}} \def\sO{{\mathscr{O}}} \def\sU{{\mathscr{U}}}  \def\sB{{\mathscr{B}}} \def\sI{{\mathscr{I}}} \def\sP{{\mathscr{P}}} \def\sV{{\mathscr{V}}}  \def\sC{{\mathscr{C}}} \def\sJ{{\mathscr{J}}} \def\sQ{{\mathscr{Q}}} \def\sW{{\mathscr{W}}}  \def\sD{{\mathscr{D}}} \def\sK{{\mathscr{K}}} \def\sR{{\mathscr{R}}} \def\sX{{\mathscr{X}}}  \def\sE{{\mathscr{E}}} \def\sL{{\mathscr{L}}} \def\sS{{\mathscr{S}}} \def\sY{{\mathscr{Y}}}  \def\sF{{\mathscr{F}}} \def\sM{{\mathscr{M}}} \def\sT{{\mathscr{T}}} \def\sZ{{\mathscr{Z}}}  \def\sG{{\mathscr{G}}} \def\sN{{\mathscr{N}}}



%%Sirve para poner la imagen de la Uni en la página frontal
%\newcommand \AlCentroPagina [1]{%
%\AddToShipoutPicture*{\AtPageCenter {%
%\makebox (0,0){\includegraphics %
%[ width =0.9\paperwidth ]{#1}}}}}



%%Cosos para los teoremas
\swapnumbers %cambia los números al inicio, se ve '1.1 Teorema', en lugar de 'Teorema 1.1'
\theoremstyle{plain}
\newtheorem{teo}{\;\;Teorema}[section]
\newtheorem{lema}[teo]{\;\;Lema}
\newtheorem{prop}[teo]{\;\;Proposición}
\newtheorem{coro}[teo]{\;\;Corolario}

\theoremstyle{definition}
\newtheorem{defn}[teo]{\;\;Definición}


\theoremstyle{remark}
\newtheorem{nota}[teo]{\;\; Nota}
\newtheorem{obs}[teo]{\;\; Observación}

%%otras cosas
\setlength{\parskip}{2ex plus 0.5ex minus 0.2ex}
\makeindex %índice alfabético
%\includeonly{intro}


\begin{document}
%las siguientes variables son definidas para ser usadas en la portada, note que no se usa \maketitle, sin embargo, las variables \title, \author y \date están definidas por defecto.

\def \myauthor{José Antonio Montero Aguilar}
\def \mythanks{}
\def \mytitle{Poliedros Regulares en el $3$-Toro}
\def \myadvisor{Dr. Daniel Pellicer Covarrubias}
%\def \coadvisor1{} \def \coadvisor2{} \def\coadvisor3{} %Usar si son necesarios
\def \myuniversity{Universidad Michoacana de San Nicolás de Hidalgo}
\def \myinstitute{Facultad de Ciencias Físico Matemáticas\\ ``Mat. Luis Manuel Rivera Gutiérrez''}
\def \degree{Licenciado en Ciencias Físico Matemáticas}
\def \degreemonth{Febrero}
\def \degreeyear{2013}
\def \degreedate{\degreemonth , \degreeyear}
\def \degreeplace{Morelia, Michoacán}

\author{\myauthor}
\title{\mytitle}
\date{\degreedate}

\frontmatter
\include{portada}
%\maketitle
\include{dedi}
\tableofcontents
\include{agrad}
\include{intro}

%\tableofcontents
\mainmatter
\include{cap1}
\include{cap2}
\include{cap3}


\backmatter
\input{conc}
\input{app_ag}
\input{app_not}
\clearpage
\addcontentsline{toc}{chapter}{Índice alfabético}
\printindex
\addcontentsline{toc}{chapter}{Bibliografía}
\bibliography{bibtesis}
\bibliographystyle{babalpha} %estilo alpha de la bibliografía "españolizado"
\end{document}