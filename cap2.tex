\chapter{El 3-toro}\label{cap:trestoro}

\section{El 3-toro y sus isometrías}

En esta sección definiremos el $3$-toro, le daremos una métrica y estudiaremos algunas de sus isometrías. Para ello daremos primero algunos resultados acerca de isometrías de $\E$ pues las isometrías del $3$-toro se pueden estudiar a través de éstas.

Los siguientes resultados dan una descripción completa de las isometrías de $\E$. El Lema \ref{lema:isomlin} es un caso particular de un teorema, el cual se puede encontrar en \cite[p. 84]{esloveno}. La prueba que aquí presentamos es una adaptación de la prueba de dicho teorema.

\begin{lema} \label{lema:isomlin}
Toda isometría de $\E$ que fija el origen es lineal.
\begin{proof}
Sea $f$ una isometría que fija al origen y $\sC=\{e_1, e_2, e_3\}$ la base canónica de $\E$. Dado que $f$ es isometría tenemos \[\Vert f(v)- f(e_i) \Vert = \Vert v-e_i \Vert\]
para todo $v \in \E$ y toda $i \in \{1,2,3\}$. Elevando al cuadrado la ecuación anterior 
\begin{eqnarray*}
\Vert f(v)- f(e_i) \Vert ^2 &=& \Vert v-e_i \Vert ^2 , \\
\Vert f(v) \Vert ^2 - 2 (f(v) \cdot f(e_i)) + \Vert f(e_i) \Vert ^2 &=& \Vert v \Vert ^2 - 2 (v \cdot e_i) + \Vert e_i \Vert ^2 ,
\end{eqnarray*}
de donde \[f(v) \cdot f(e_i) = v \cdot e_i \] para todo $v \in \E$ y toda $i \in \{1,2,3\}$, pues $\Vert f(v) \Vert = \Vert v \Vert$ y $\Vert f(e_i) \Vert = \Vert e_i \Vert$ ya que $f$ es una isometría que fija el origen.

Dado que $f$ es isometría, $\{f(e_1), f(e_2), f(e_3)\}$ es una base ortonormal de $\E$ y entonces \[f(v)=\sum_{i=1}^{3} (f(v) \cdot f(e_i) ) f(e_i) = \sum_{i=1}^{3} (v \cdot e_i ) f(e_i)\] para todo $v \in \E$.

Sea $T: \E \to \E$ la transformación lineal definida por $T(e_i)=f(e_i)$. Ya que $\sC$ es base ortonormal tenemos 
\begin{eqnarray*}
T(v)&=&T(\sum_{i=1}^{3} (v \cdot e_i )(e_i)) \\
&=&\sum_{i=1}^{3} (v \cdot e_i )T(e_i) \\
&=&\sum_{i=1}^{3} (v \cdot e_i )f(e_i)\\
&=&f(v)
\end{eqnarray*}
para todo $v \in \E$ y por lo tanto, $f$ es lineal.
\end{proof}
\end{lema}

\begin{prop} \label{prop:isomtraslin}
Toda isometría $g$ de $\E$ se escribe de forma única como $g=ft$ donde $t$ es una traslación y $f$ una isometría lineal.
\begin{proof}
Sea $g \in Isom(\E)$ y $\V{v}=g^{-1}(0)$, sea $t$ la traslación por $-\V{v}$. Tenemos que $gt^{-1}(0)=0$ y por Lema \ref{lema:isomlin} $gt^{-1}=f$ con $f$ isometría lineal, y por lo tanto $g=ft$.

Supongamos ahora que $f_1 t_1 = g = f_2 t_2$, entonces $ f_{2}^{-1} f_{1} = t_2 t_{1}^{-1}$. El lado izquierdo es lineal y entonces fija el origen así que $t_1=t_2$ y por lo tanto $f_1=f_2$.
\end{proof}
\end{prop}

El siguiente lema, además de darnos una manera sencilla de probar la Proposición \ref{prop:isomlintras}, por sí mismo tiene importancia pues nos dice que el conjugado de una traslación es otra traslación. 
 
\begin{lema} \label{lema:trnorm}
Sea $G \leq Isom(\E)$ un grupo de isometrías y $T \leq G$ el grupo de traslaciones de $G$, entonces $T \triangleleft G$.
\begin{proof}
Sea $g \in G$ y $t$ una traslación en $T$, digamos por $\V{v}$. Por la Proposición \ref{prop:isomtraslin} $g=f_g t_g$ para alguna $f_g$ lineal y $t_g$ traslación. Sea $x \in \E$, entonces
\begin{eqnarray*}
(gtg^{-1})(x) & = & (f_g t_g t t_{g}^{-1} f_{g}^{-1})(x) \\
& = & (f_g t)( f_{g}^{-1}(x)) \\
& = & f_g ( f_{g}^{-1}(x)+\V{v}) \\
& = & x + f_g (\V{v}) 
\end{eqnarray*}
y esto para $x$ arbitrario, por lo tanto $gtg^{-1} \in T$ y $T \triangleleft G$.
\end{proof}
\end{lema}

\begin{prop} \label{prop:isomlintras}
Toda isometría $g$ de $\E$ se escribe de forma única como composición de una isometría lineal $f$ por una traslación $t$, es decir, $g=tf$. Además, si $f_1 t_1=g=t_2 f_2$ con $f_1$, $f_2$ lineales y $t_1$, $t_2$ traslaciones, entonces $f_1=f_2$.
\begin{proof}
Por la Proposición \ref{prop:isomtraslin} y el Lema \ref{lema:trnorm} $g=f_1t_1$ y $$gf_{1}^{-1}=f_{1} t_1 f_{1}^{-1}=t_2,$$ por lo tanto $g=t_2f_1$. La unicidad se sigue de la unicidad de $f_1$ y $t_1$ en la Proposición \ref{prop:isomtraslin}.
\end{proof}
\end{prop}

\begin{prop} \label{prop:Lin}
Existe un homomorfismo de grupos $\mathbf{Lin}$ de $Isom(\E)$ en $O(3)$, el grupo de isometrías lineales de $\E$, cuyo núcleo es $Tras(\E)$ el subgrupo de traslaciones.
\begin{proof}
Definamos $\mathbf{Lin}(g)=f$, donde $f$ es la única isometría lineal tal que $g=tf$. $\mathbf{Lin}$ es homomorfismo pues si $g_1,g_2 \in Isom(\E)$ con $g_1=t_1 f_1 $ y $g_2=t_2 f_2 $ entonces 
\begin{eqnarray*}
g_1 g_2 & = & t_1 f_1 t_2 f_2 \\
& = & t_1 f_1 t_2 f_{1}^{-1} f_1 f_2 \\
& = & t_1 \bar{t}_2 f_1 f_2 \;\;\;\; \textrm{ donde $\bar{t}_2=f_1 t_2 f_1^{-1}$ y por el Lema  \ref{lema:trnorm} es una traslación }
\end{eqnarray*} 
y entonces $\mathbf{Lin}(g_1 g_2)= f_1 f_2 = \mathbf{Lin}(g_1) \mathbf{Lin}(g_2)$
\end{proof}
\end{prop}

En lo sucesivo, si $g \in Isom(\E)$, denotaremos por $g'$ a $\mathbf{Lin}(g)$. Observemos que la notación $g'$ está justificada, pues $g$ es una función diferenciable de $\E$ en sí mismo, y $g'$ coincide con la transformación lineal asociada a la derivada de $g$.

La Proposición \ref{prop:Lin} y el Primer Teorema de Isomorfismo de grupos nos dicen que existe un isomorfismo $\overline{\mathbf{Lin}}: Isom(\E)/Tras(\E) \to O(3)$, es decir, que cada isometría lineal es un representante de una familia de isometrías que comparten algunas propiedades. Esto se verá reflejado en los siguientes resultados.

\begin{coro} \label{coro:trasconj}
Si $g$ es una isometría de $\E$ y $t$ es la traslación por $\V{v}$ entonces $g t g^{-1}$ es la traslación por $g'(\V{v})$.
\begin{proof}
Se sigue de la demostración del Lema \ref{lema:trnorm}.
\end{proof}
\end{coro}

\begin{coro} \label{coro:bastaprima}
Sean $\T$ un grupo de traslaciones de $\E$ y $\cN(\T)$ el normalizador de $\T$ en $Isom(\E)$. Si $g_1$ y $g_2$ son isometrías de $\E$ con $g_{1}'=g_{2}'$ entonces $g_1 \in \cN(\T)$ si y sólo si $g_2 \in \cN(\T)$, en particular, $g$ normaliza a $\T$ si y sólo si $g'$ lo hace.
\begin{proof}
Del Corolario \ref{coro:trasconj}, si $t \in \T$, $g_{1} t g_{1}^{-1}=g_{2} t g_{2}^{-1}$. La segunda parte se sigue de que $(g')'=g'$.
\end{proof}
\end{coro}


A continuación entraremos en el estudio del $3$-Toro, daremos una métrica y veremos que muchas de sus isometrías están dadas por isometrías de $\E$.

\begin{defn} \index{3-Toro}
Sea $\T = \langle t_{\V{v}_1}, t_{\V{v}_2}, t_{\V{v}_3} \rangle \leq Isom(\bE^3)$ un grupo de traslaciones con $\V{v}_1, \V{v}_2, \V{v}_3$ vectores linealmente independientes. Definimos el \emph{$3$-toro} $\Tt$ por: $$ \Tt := \E/\T = \left\lbrace [ x ]_{\T} : x \in \E \right\rbrace. $$

Escribiremos únicamente $\TT$ cuando no haya confusión respecto al grupo de traslaciones $\T$. 

Si $e$ es la métrica euclideana en $\E$ definimos la función $e_{\T} : \Tt \times \Tt \to \bR $ por: $$ e_{\T}\left([x]_{\T},[y]_{\T}\right)= \inf \left\lbrace e(t_1(x),t_2(y)) : t_1, t_2 \in \T \right\rbrace. $$ 
\end{defn}

El siguiente resultado lo daremos sin demostración, una prueba puede encontrarse en \cite[Teo 5.35]{yale}.

\begin{teo}
Sea $\T = \langle t_{\V{v}_1}, t_{\V{v}_2}, t_{\V{v}_3} \rangle \leq Isom(\bE^3)$ un grupo de traslaciones con $\V{v}_1, \V{v}_2, \V{v}_3$ vectores linealmente independientes. Entonces $\T$ es un grupo discreto, es decir, las órbitas no tienen puntos de acumulación.
\end{teo}

\begin{lema}\label{lema:met}
Si $x,y \in \E$ y $t_0 \in \T$, entonces $$e_{\T}\left(\left[ x \right]_{\T}, \left[ y \right]_{\T}\right) = \min \left\lbrace e\left(t_0(x),t(y)\right) : t \in \T \right\rbrace. $$

\begin{proof}
Observemos que si $t_1, t_2 \in \T$ y $x,y \in \E$, entonces $e(t_1(x),t_2(y))= e(x,t_{1}^{-1}t_2(y))$  por ser $t_1$ isometría. Así que tenemos $$e_{\T}\left(\left[ x \right]_{\T}, \left[ y \right]_{\T}\right) = \inf \left\lbrace e\left(x,t(y)\right) : t \in \T \right\rbrace. $$
Ahora probemos que es mínimo. Sea $d = \inf \left\lbrace e\left(x,t(y)\right) : t \in \T \right\rbrace$. Probemos que existe $s \in \T $ tal que $d=e(x, s(y))$. Para cada $n \in \bN$ existe $s_n \in \T$ tal que $s_n (y)\in B_{d+\frac{1}{n}}(x)$, la bola de radio $d+\frac{1}{n}$ con centro en $x$, así que, o bien existe $s \in \T$ con $d=e(x,s(y))$, o bien hay una infinidad de elementos de la órbita de $y$ en el conjunto $K= \{ z \in \E : d \leq e(x,z) \leq d+1 \}$, pero esto último es imposible pues $K$ es compacto y entonces la órbita de $y$ tendría un punto de acumulación.
\end{proof}
\end{lema} 


\begin{prop} \label{prop:met} \index{3-Toro!métrica}
La función $e_{\T}$ es una métrica en $\Tt$.
\begin{proof}
Claramente $e_{\T} \geq 0$. La simetría de $e_{\T}$ se sigue de la simetría de $e$. Gracias al Lema \ref{lema:met} $e_{\T}([x]_{\T},[y]_{\T})= 0$ si y sólo si $[x]_{\T}=[y]_{\T}$.

Sean $[x]_{\T}, [y]_{\T}, [z]_{\T} \in \TT$. Por el Lema \ref{lema:met} tenemos que $$ e_{\T}([x]_{\T},[z]_{\T})+e_{\T}([z]_{\T},[y]_{\T}) = e(x,t_1(z))+e(t_1(z),t_2(y))$$ para algunos $t_1, t_2 \in \T$, luego \[ e(x,t_1(z))+e(t_1(z),t_2(y)) \geq e(x,t_2(y) ) \geq e_{\T}([x]_{\T},[y]_{\T}) \qedhere \]
\end{proof}
\end{prop}

Una vez que $\TT$ tiene métrica tiene sentido preguntarnos por sus isometrías, es decir, funciones que preserven la métrica. El siguiente resultado nos dice cuáles isometrías de $\E$ inducen isometrías en $\TT$. Se puede probar que toda isometría de $\TT$ se levanta a una isometría de $\E$, pero este resultado no es relevante para este trabajo.

\begin{prop} \label{prop:norm}
Si $g$ es isometría de $\E$, $g$ induce una isometría $\hat{g}$ de $\TT$ si y sólo si $g \in \cN (\T)$, el normalizador de $\T$ en $Isom(\E)$.
\begin{proof}
Supongamos que $g$ induce una isometría en $\TT$. Sea $t \in \T$ la traslación por $\V{v}$. Por el Corolario \ref{coro:trasconj} $gtg^{-1}=\bar{t}$, la traslación por $g'(\V{v})$. Además, ya que $g$ induce una isometría en $\TT$, $g$ manda órbitas en órbitas. De aquí se sigue que si $tg^{-1}(x) \in [g^{-1}(x)]_{\T}$, entonces $\bar{t}(x)=gtg^{-1}(x) \in [x]_{\T}$, para $x$ arbitrario en $\E$, por lo tanto $\bar{t} \in \T$ y $g \T g^{-1}=\T$.

Supongamos ahora que $g \in \cN(\T)$, entonces, si $t \in \T$ existe un único $\bar{t} \in \T$ tal que $gt=\bar{t}g$. Definamos $\hat{g}:\Tt \to \Tt$ por $\hat{g}([x]_{\T}) = [g(x)]_{\T}$. Probemos que $\hat{g}$ está bien definida: Si $[x]_{\T}=[y]_{\T}$, entonces $x=t(y)$ para alguna $t$ en $\T$, y así $g(x)=gt(y)=\bar{t}g(y)$ y por lo tanto $[g(x)]_{\T}=[g(y)]_{\T}$. Claramente $\hat{g}$ es suprayectiva. 

Para probar que $\hat{g}$ es isometría basta observar que $e(x,t(y))= e(g(x),gt(y))=e(g(x),\bar{t}g(y))$ y entonces $$\inf \left\lbrace e\left(x,t(y)\right) : t \in \T \right\rbrace = \inf \left\lbrace e\left(g(x),\bar{t}g(y)\right) : \bar{t} \in \T \right\rbrace $$ y por lo tanto $e_{\T}([x]_{\T},[y]_{\T})=e_{\T}(\hat{g}([x]_{\T}),\hat{g}([y]_{\T}))$. 
\end{proof}
\end{prop}

En virtud de la Proposición \ref{prop:norm} y de los Corolarios  \ref{coro:trasconj} y \ref{coro:bastaprima} estudiaremos, dada una isometría $g$, a qué grupos de traslaciones normaliza $g'$, pues esto nos dirá cuáles son los $3$-toros en los que $g$ induce una isometría.

\section{Retículas de puntos}\label{sec:latices}

El objetivo de esta sección es estudiar las \emph{retículas de puntos}, las cuales son objetos geométricos que nos permitirán entender mejor al $3$-Toro.

\begin{defn} \index{Retícula de puntos}
Una \emph{retícula de puntos} $\L_{\T}$ en $\E$ es la órbita del $0$ bajo un grupo discreto de traslaciones $\T$. Escribiremos únicamente $\L$ si no hay confusión respecto al grupo $\T$. La retícula se dirá $k$-dimensional si hay $k$ vectores linealmente independientes en ella (Fig \ref{fig:latbid} y \ref{fig:lattrid}).
\end{defn}

%\begin{figure}
%\begin{minipage}{0.45\linewidth}
%\centering
%\def\svgwidth{\linewidth}
%\input{latbid.pdf_tex}
%\caption{Latiz bidimensional}
%\label{fig:latbid}
%\end{minipage}
%\hspace*{0.5cm}
%\begin{minipage}{0.45\linewidth}
%\centering
%\def\svgwidth{\linewidth}
%\input{lattrid.pdf_tex}
%\caption{Latiz tridimensional}
%\label{fig:lattrid}
%\end{minipage}
%\end{figure}

\begin{figure}[h]
\centering
\begin{scriptsize}
\begin{subfigure}[b]{0.45\textwidth}
  \centering
  \def\svgwidth{\linewidth}
  \input{latbid.pdf_tex}
  \caption{Retícula bidimensional}
  \label{fig:latbid}
\end{subfigure}%
\qquad
\begin{subfigure}[b]{0.45\textwidth}
  \centering
  \def\svgwidth{\linewidth}
  \input{lattrid.pdf_tex}
  \caption{Retícula tridimensional}
  \label{fig:lattrid}
\end{subfigure}
\end{scriptsize}
\caption{}
\end{figure}

Observemos que hay un isomorfismo de grupos entre $\T$ y $\L_{\T}$ si pensamos a $\L_{\T}$ como subgrupo de $(\bR^3,+)$, así que estudiar $\L_{\T}$ es una forma de estudiar $\T$ y por lo tanto $\TT$. Observemos también que $\L_{\T}$ depende únicamente de $\T$ y no de los generadores, así que gran parte de nuestro estudio se concentrará en encontrar generadores adecuados para $\T$ a través de las propiedades geométricas de $\L_{\T}$. Trataremos de manera indistinta a los elementos de $\L_{\T}$ como puntos o vectores, sin embargo, los denotaremos $p,q,w,v,$ \ldots cuando éstos sean pensados como puntos y $\V{p},\V{q},\V{v},\V{w}$ \ldots cuando lo sean como vectores.

\begin{prop} \label{prop:latinv}
Sean $\T$ un grupo de traslaciones generado por 3 traslaciones linealmente independientes, $g \in Isom(\E)$. Entonces $g \in \cN(\T)$ si y sólo si $g'$ preserva a $\L_{\T}$.
\begin{proof}
Supongamos que $g \in \cN(\T)$. Por la linealidad de $g'$ basta probar que $g'(\V{v}_i) \in \L$ para $i \in \{1,2,3\}$, pero esto último se sigue del Corolario \ref{coro:trasconj} pues $g \in \cN(\T)$.

Supongamos ahora que $g'$ preserva a $\L_{\T}$. Sea $t \in \T$ la traslación por $\V{v}$. Por el Corolario \ref{coro:trasconj} $gtg^{-1}$ es la traslación por $g'(\V{v})$, pero $g'(\V{v}) \in \L_{\T}$ por lo que $gtg^{-1} \in \T$ así que $g \in \cN(\T)$.
\end{proof}
\end{prop}

\subsection*{Retículas invariantes bajo reflexiones}

La Proposición \ref{prop:latinv} nos dice que es equivalente estudiar isometrías que normalizan a $\T$ e isometrías que dejan invariante a $\L_{\T}$. Ya que los grupos de simetrías de los poliedros que estudiaremos son generados por reflexiones, estudiaremos cómo deben ser las retículas que quedan invariantes bajo una reflexión.

En lo sucesivo $\Pi$ será un plano en $\E$, $R$ la reflexión respecto a $\Pi$, $\L$  la retícula de un grupo $\T = \langle t_{\V{v}_1}, t_{\V{v}_2}, t_{\V{v}_3} \rangle$ con $\V{v}_1, \V{v}_2, \V{v}_3$ linealmente independientes tal que $R(\L)=\L$. Si $p$ es un punto en $\E$, $\pi(p)$ es su proyección a $\Pi$, y $\pi(\L)$ es la proyección de $\L$ en $\Pi$.

Para nuestros fines asumiremos siempre que $\L \cap \Pi \neq \emptyset$.

\begin{prop} \label{prop:latintplaspans}
Sea $\L$ una retícula como la descrita antes, entonces existen dos vectores linealmente independientes $\v_1$, $\v_2$ de tal forma que si $q \in \L \cap \Pi$, entonces $\L \cap \Pi= q + \langle \v_1,\v_2 \rangle$.
\begin{proof}
Sin pérdida de generalidad podemos suponer que $q=0$. Observemos que si $u$, $v$ son puntos en $\L$ de tal forma que $\pi(u)$ y $\pi(v)$ son linealmente independientes, entonces $u+R(u)$ y $v+R(v)$ están en $\Pi$ y son linealmente independientes, así que $\L$ es bidimensional.

Si $\V{v}_1$ y $\V{v}_2$ son dos vectores linealmente independientes en $\L \cap \Pi$, observemos que $\langle \V{v}_1, \V{v}_2 \rangle$ nos induce una teselación de $\Pi$ en paralelogramos congruentes con el paralelogramo $P$ formado por $0$,$\V{v}_1$,$\V{v}_2$ y $\V{v}_1+\V{v}_2$ (Fig. \ref{fig:plantespar}).

\begin{figure}[!hbt]
\centering
\def\svgwidth{8cm}
\input{plantespar.pdf_tex}
\caption{$\Pi$ teselado por paralelogramos.}
\label{fig:plantespar}
\end{figure}

Entonces $\Pi=\cL_{\bR}(\v_1,\v_2)$ y así, cada punto de $\L \cap \Pi$  lo podemos escribir como $\alpha_1 \v_1 + \alpha_2 \v_2$ con $\alpha_1, \alpha_2 \in \bR$, además, para los puntos del interior de $P$ podemos pedir que $\alpha_i \in (0,1), i \in \{1,2\}$. 

Tomemos $\v_1$ y $\v_2$ de tal forma que $P$ (y por lo tanto, todos los demás) tenga un número mínimo $n$ de puntos de $\L \cap \Pi$ en su interior. Si $P$ contiene puntos de $\L$ en sus lados podemos tomar $\v'_1$ y $\v'_2$ como aquellos puntos de coordenada $\alpha_1$ y $\alpha_2$ mínima, respectivamente (Fig. \ref{fig:ptoslados}). Así que podemos suponer que $P$ no contiene puntos en sus lados. 

Probemos entonces que $n=0$, es decir, que todos los puntos de $\L \cap \Pi$ son vértices de los paralelogramos. Supongamos que $n \geq 1$ y sea $\V{w}$ un punto de en el interior de $P$, entonces el paralelogramo $P'$ formado por $0$,$\v_1$,$\V{w}$ y $\v_1 + \V{w}$ (Fig. \ref{fig:ptosint}) tiene menos puntos en su interior (pues por la elección de $w$ tiene a lo más tantos puntos como $P$ pero ahora $w$ ya no está en el interior). De aquí se sigue que $n = 0$. Por lo tanto todos los puntos son vértices de los paralelogramos y $\L \cap \Pi=\langle \v_1, \v_2 \rangle$. 
\end{proof} 
\end{prop}

%\begin{figure}
%\begin{minipage}{0.45\linewidth}
%\centering
%\def\svgwidth{\linewidth}
%\input{prllg1.pdf_tex}
%\caption{Puntos en los lados}
%\label{fig:ptoslados}
%\end{minipage}
%\hspace*{0.5cm}
%\begin{minipage}{0.45\linewidth}
%\centering
%\def\svgwidth{\linewidth}
%\input{prllg2.pdf_tex}
%\caption{Puntos en el interior}
%\label{fig:ptosint}
%\end{minipage}
%\end{figure}
%

\begin{figure}[h]
\centering
\begin{scriptsize}
\begin{subfigure}[b]{0.45\textwidth}
  \centering
  \def\svgwidth{\linewidth}
  \input{prllg1.pdf_tex}
  \caption{Puntos en los lados}
  \label{fig:ptoslados}
\end{subfigure}%
\qquad
\begin{subfigure}[b]{0.45\textwidth}
  \centering
  \def\svgwidth{\linewidth}
  \input{prllg2.pdf_tex}
  \caption{Puntos en el interior}
  \label{fig:ptosint}
\end{subfigure}
\end{scriptsize}
\caption{}
\end{figure}

\begin{lema}
Sean $\L$ una retícula asociada a un grupo $\T$ y $\Pi$ un plano tal que $\L$ es invariante bajo la reflexión con respecto a $\Pi$. Entonces existe un punto $p \in \L  \setminus \Pi$ cuya distancia a $\Pi$ es mínima.
\begin{proof}
Supongamos que tal $p$ no existe, entonces para cada $n \in \bN$ existe un punto $p_n \in \L$ tal que $e(p_n, \Pi) < \frac{1}{2^{n+1}}$, es decir, exiten vectores $\vec{v}_n$ (el vector de la traslación que manda $p_n$ en $R(p_n)$) en $\L$ con $|v_n| < \frac{1}{2^n}$ y así el punto $x=\sum_{n=1}^{\infty}v_n(0)$ es un punto de acumulación de $\L$ lo cual es una contradicción.
\end{proof}
\end{lema}

\begin{lema}\label{lema:capas}
Sean $\L$ la retícula asociada a un grupo $\T$ y $\Pi$ un plano que contiene al origen tal que $\L$ es invariante bajo la reflexión con respecto a $\Pi$. Si $p$ es un punto de $\L$ a distancia mínima positiva de $\Pi$ entonces $$\L=\bigcup_{k \in \bZ}\L_{k}$$ donde $\L_{k}=(\L \cap \Pi)+k\V{p}$. A $\L_k$ le llamaremos la \emph{capa} $k$ de $\L$.

 
\begin{proof}
Sean $\v_1$ y $\v_2$ vectores con $\langle \v_1, \v_2 \rangle = \L \cap \Pi$. Sea $q$ un punto en $\L$, sin pérdida de generalidad, $q$ y $p$ están del mismo lado del espacio con respecto a $\Pi$. Sean $\alpha_1, \alpha_2, \alpha_3 \in \bR$ tales que $q=\alpha_1 \v_1 + \alpha_2 \v_2+ \alpha_3 \V{p}$. Observemos que $\alpha_3 \in \bZ$, pues de lo contrario  $q-\lfloor \alpha_3 \rfloor \V{p}$ sería un punto de $\L$ entre los planos $\Pi$ y $\Pi+\V{p}$ (Fig. \ref{fig:planos1}), lo cual contradice que $p$ tenía distancia mínima positiva. Que $\alpha_1, \alpha_2 \in \bZ$ se sigue de que $q-\alpha_3 \V{p}=\alpha_1 \v_1 + \alpha_2 \v_2 \in \L \cap \Pi$.
\end{proof}
\end{lema}

\begin{figure}[!hbt]
\begin{center}
\begin{small}
\def\svgwidth{12cm}
\input{planos1.pdf_tex}
\caption{Si $\lfloor \alpha_3 \rfloor \neq \alpha_3$}
\label{fig:planos1}
\end{small}
\end{center}
\end{figure}

En la Proposición \ref{prop:latintplaspans} construimos dos vectores $\v_1, \v_2$ de tal forma que $ \L \cap \Pi = \langle \v_1, \v_2 \rangle$. Otra forma de entender el Lema \ref{lema:capas} es como la construcción de un tercer vector $\v_3$ de tal forma que $\L=\langle \v_1, \v_2, \v_3 \rangle$. En la Proposición \ref{prop:IntYProyCoinciden} veremos que, en el caso que $\pi(\L) = \L \cap \Pi$, podemos tomar nuestro tercer generador ortogonal al plano $\Pi$; por otro lado, en la Proposición \ref{prop:IntYProyNoCoinciden} veremos que si $\pi(\L) \neq \L \cap \Pi$ tenemos también un vector $\w$ ortogonal a $\Pi$ de tal forma que $\langle \v_1, \v_2, \w \rangle$, si bien no es todo $\L$, es un subgrupo de índice $2$.


\begin{prop} \label{prop:IntYProyCoinciden}
Sean $\L$ una retícula de un grupo $\T$ y $\Pi$ un plano que contiene al origen tal que $\L$ es invariante bajo la reflexión con respecto a $\Pi$. Si $\pi(\L) = \L \cap \Pi$, entonces existe un vector $\w \in \L$ ortogonal a $\Pi$ tal que $\L = \langle \v_1, \v_2, \w \rangle$ donde $\langle \v_1, \v_2 \rangle = \L \cap \Pi$.
\begin{proof}
Sean $p \in \L$ un punto de distancia mínima positiva a $\Pi$ y $\w=\V{p}-\pi(\V{p})$. Por el Lema \ref{lema:capas} $\L=\bigcup_{k \in \bZ}\L_{k}$ con $\L_{k}=(\L \cap \Pi)+k\V{p}$ pero
$$(\L \cap \Pi)+k\V{p}=(\L \cap \Pi)+k(\pi(\V{p}) + \w) = (\L \cap \Pi) + k\w.$$
Por lo tanto $\L = \langle \v_1, \v_2, \w \rangle$.
\end{proof}
\end{prop}


\begin{prop} \label{prop:IntYProyNoCoinciden}
Sean $\L$ una retícula de un grupo $\T$ y $\Pi$ un plano por el origen tal que $\L$ es invariante bajo la reflexión con respecto a $\Pi$. Si $\pi(\L) \neq \L \cap \Pi$ entonces existen un vector $\w \in \L$ ortogonal a $\Pi$ tal que el índice de $\langle \v_1, \v_2, \w \rangle$ en $\L$ es $2$.
\begin{proof}
Sea $p$ un punto de distancia mínima positiva a $\Pi$ y sea $\w=\V{p}-R(\V{p})$. Para cada $k \in \bZ$ sea $\L_k$ como en el Lema \ref{lema:capas}. Basta probar que $\omega \in L_2$, pues una clase lateral (aquella que contiene a $\w$) será la de los puntos en capas con índice par y la otra (la que contiene a $\V{p}$) será la de los puntos en capas de índice impar. Observemos que tanto $-p$ como $R(p)$ son puntos en $\L_{-1}$, y por lo tanto $R(p)=-p+v$ con $v \in \L_0$ así que $$\w=\V{p}-R(\V{p})=\V{p}-(-\V{p}+\V{v})=\V{v}+2\V{p}$$ y por lo tanto $\w \in L_2$.
\end{proof}
\end{prop}

\begin{coro} \label{coro:PuntosMedios}
Sean $\L$ una retícula asociada a un grupo $\T$ y $\Pi$ un plano tal que $\L$ es invariante bajo la reflexión con respecto a $\Pi$. Si $\L \cap \Pi \neq \emptyset$, entonces todo punto de $\L$ se proyecta o bien en un punto de $\L \cap \Pi$ o bien en el punto medio entre dos puntos de $\L \cap \Pi$.
\begin{proof}
Si $\pi(\L)= \L \cap \Pi$ no hay nada que hacer. Si $\pi(\L) \neq \L \cap \Pi$ sean $\v_1$, $\v_2$, $\v_3$ y $\w$ son como en la Proposición \ref{prop:IntYProyNoCoinciden}. Basta observar que si $p=n\v_1 + m\v_2 + k\w $ entonces $\pi(p)=n\v_1 + m\v_2$ y si $p=\v_3 + n\v_1 + m\v_2 + k\w $ entonces $\pi(p)$ es punto medio entre $n\v_1 + m\v_2$ y $2\v_3 + n\v_1 + m\v_2 - \w$.
\end{proof}
\end{coro}

El Corolario anterior motiva la siguiente definición:

\begin{defn} \index{Retícula de puntos!de traslación paralela}
Sea $\T$ un grupo generado por 3 traslaciones linealmente independientes de tal forma que $\L_{\T}$ es invariante bajo una reflexión respecto a un plano $\Pi$. Diremos que $\L_{\T}$ es \emph{de traslación paralela} con respecto a $\Pi$ si todos los puntos de $\L_{\T}$ se proyectan en puntos de $\L_{\T} \cap \Pi$.
\end{defn}

En los resultados de esta sección hemos dado generadores $\v_1, \v_2, \v_3$ para $\L$ de tal forma que $\v_1$ y $ \v_2 \in \Pi$ y $\v_3 \notin \Pi$. Además exhibimos un vector $\w$ ortogonal a $\Pi$ donde $|\w|$ es mínimo entre los vectores de $\L$ ortogonales a $\Pi$ e incluso probamos en la Proposición \ref{prop:IntYProyCoinciden} que si $\L \cap \Pi = \pi(\L)$ podemos tomar $\v_3 = \w$. Probamos también que los puntos $\L$ son esencialmente de dos tipos: los que se proyectan en puntos de $\L \cap \Pi$ y los que se proyectan en puntos medios entre éstos. En el siguiente capítulo usaremos estos resultados para clasificar las retículas que se quedan invariantes bajo los grupos de los poliedros regulares. 

Los resultados \ref{prop:latintplaspans}, \ref{prop:IntYProyCoinciden}, \ref{prop:IntYProyNoCoinciden} y \ref{coro:PuntosMedios} tienen sus análogos bidimensionales (es decir, considerando que $\L$ es una retícula bidimensional que es invariante bajo la reflexión de una recta $\Pi$) y la pruebas son esencialmente las mismas. Así, si $\L$ es una retícula bidimensional invariante bajo la reflexión con respecto a una recta $\Pi$, diremos que $\L$ es de \emph{traslación paralela} respecto a $\Pi$ si $\pi(\L) = \Pi \cap \L$.



\subsection*{Tres Retículas Importantes}


A continuación describiremos tres retículas que resultarán importantes y conviene estudiarlas con un poco de detalle

\begin{description}
\item[Retícula de Cubos.]\index{$\cl$} Corresponde a la retícula dada por los vértices de una teselación de cubos. Un ejemplo es la retícula generada por los vectores $(1,0,0),(0,1,0), (0,0,1)$, la cual denotaremos $\cl$. Sus  puntos son los de la forma $(m,n,k)$ con $m,n,k \in \bZ$.

\item[Retícula centrada en las caras.]\index{$\fcl$} Corresponde a la retícula que consta de los vértices de una teselación de cubos y de los puntos medios de las caras de dicha teselación. Un ejemplo es la generada por los vectores $(1,1,0),(1,0,1), (0,1,1)$ la cual denotaremos $\fcl$. Ésta corresponde con todos los puntos de coordenadas enteras cuya suma es par.

\item[Retícula centrada en los cubos.]\index{$\bcl$} Corresponde a la retícula dada por los vértices de una teselación de cubos y los centros de cada uno de los cubos. Un ejemplo es la generada por los vectores $(2,0,0),(0,2,0),(1,1,1)$ la cual denotaremos $\bcl$. Consta de todos los puntos de coordenadas enteras donde todas sus coordenadas tienen la misma paridad.
\end{description}

La notación de $\cl$, $\fcl$ y $\bcl$ fue tomada de \cite[\S 6D]{ARP}.

%\begin{figure}[b]
%\begin{minipage}{0.45\linewidth}
%\centering
%\def\svgwidth{5cm}
%\input{cl.pdf_tex}
%\caption{$\cl$}
%\label{fig:cl}
%\end{minipage}
%\hspace*{0.5cm}
%\begin{minipage}{0.45\linewidth}
%\centering
%\def\svgwidth{5cm}
%\input{fcl.pdf_tex}
%\caption{$\fcl$}
%\label{fig:fcl}
%\end{minipage}
%\end{figure}
%
%\begin{figure} 
%\begin{center}
%\def\svgwidth{5cm}
%\input{bcl.pdf_tex}
%\caption{$\bcl$}
%\label{fig:bcl}
%\end{center}
%\end{figure}


\begin{figure}[h]
\begin{scriptsize}
\begin{center}
\begin{subfigure}[b]{0.3\textwidth}
  \centering
  \def\svgwidth{\linewidth}
  \input{cl.pdf_tex}
  \caption{$\cl$}
  \label{fig:cl}          
\end{subfigure}%
\qquad
\begin{subfigure}[b]{0.3\textwidth}
  \centering
  \def\svgwidth{\linewidth}
  \input{fcl.pdf_tex}
  \caption{$\fcl$}
  \label{fig:fcl}
\end{subfigure}
\qquad
\begin{subfigure}[b]{0.3\textwidth}
  \centering
  \def\svgwidth{\linewidth}
  \input{bcl.pdf_tex}
  \caption{$\bcl$}
  \label{fig:bcl}
\end{subfigure}
\end{center}
\end{scriptsize}
\caption{}
\end{figure}
