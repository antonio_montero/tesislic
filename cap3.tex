\chapter{Poliedros Regulares en el $3$-Toro}\label{cap:polregT3}

En este capítulo abordaremos el problema principal del trabajo: decidir para qué grupos $\T$ generados por $3$ traslaciones linealmente independientes un poliedro regular $\P$ realizado en $\E$ tiene realización discreta en $\Tt=\E / \T$.

\section{Poliedros Finitos}\label{polfinT3}

En esta sección estudiaremos las posibles realizaciones de los poliedros finitos en $\E$. Como sabemos (ver \S \ref{fampol}), estos se dividen en $3$ familias según su grupo de simetrías: la familia del tetraedro, la familia del octaedro y la familia del icosaedro.

Por lo estudiado en la Sección \ref{sec:RPR} el grupo de simetrías de un representante de cada familia está generado por tres reflexiones $\r_0, \r_1, \r_2$ respecto a planos $\Pi_0, \Pi_1, \Pi_2$ respectivamente. Para determinar los grupos de traslaciones $\T$ tales que los poliedros regulares finitos admiten realización en $\Tt$, el análisis consistirá en ver cómo deben ser las proyecciones de la retícula $\L_{\T}$ en los planos $\Pi_0$ y $\Pi_2$ para que ésta sea invariante bajo las reflexiones.

\begin{obs}\label{obs:lineal}
Gracias a que las isometrías con las que haremos el análisis son lineales podemos hacer ciertas suposiciones acerca de los generadores de la retícula. Usaremos principalmente que si una retícula $\L=\langle \v_1, \v_2, \v_3  \rangle$ es preservada bajo un grupo $G$ de isometrías lineales, entonces la retícula $a \L := \langle a \v_1, a \v_2, a \v_3  \rangle $ también es preservada por $G$ para cualquier $a \in \bR$.
\end{obs}

\begin{nota} \label{notodas}
A pesar de que la Observación anterior nos dice que si una retícula $\L_{\T}$ es preservada por un grupo, entonces todos sus múltiplos escalares también los son, para los poliedros finitos impondremos la siguiente condición. Para cada poliedro $\P$ daremos restricciones acerca de cuáles múltiplos escalares de las retículas nos interesan, de modo que evitemos que en el cociente $\E / \T$ se identifiquen vértices, aristas o caras del poliedro, pues esto nos daría realizaciones de objetos combinatoriamente distintos a $\P$. Intuitivamente, buscamos que cada uno de los toros sea suficientemente grande para que quepa el poliedro.
\end{nota}

Gracias a los resultados obtenidos en la Sección \ref{sec:latices}, si una retícula $\L$ es invariante bajo una reflexión respecto al plano $\Pi$ sus puntos son de dos tipos: los que proyectan en puntos de $\L \cap \Pi$ y los que se proyectan en puntos medios de éstos. Por tal motivo, es posible estudiar la retícula únicamente analizando sus proyecciones, así que introduciremos simbología para este análisis. Cuando proyectemos $\L$ en $\Pi$ dibujaremos un punto ($\bullet$) para representar a los puntos que se proyectan en puntos de $\L \cap \Pi$; usaremos estrella ($\star$) para los puntos de $\pi(\L)$ que representan a puntos de $\L$ que no se proyectan en puntos de $\L \cap \Pi$. Con esta convención los puntos ($\bullet$) siempre representan a un punto de $\L \cap \Pi$ y los puntos ($\star$) nunca representan a un punto de $\L \cap \Pi$ y siempre están en puntos medios de segmentos entre puntos ($\bullet$). Los ejemplos en las figuras \ref{ejem1}, \ref{ejem2}, \ref{ejem3} y \ref{ejem4} ejemplifican esta simbología.

Usaremos la notación de la Sección \ref{sec:latices} para los generadores del grupo $\T$, es decir $\L = \langle \v_1, \v_2, \v_3 \rangle $ con $\L \cap \Pi = \langle \v_1, \v_2 \rangle$ y $\w \in \L$ es ortogonal a $\Pi$ y de norma mínima en $\Pi^{\perp} \cap \L$.  Denotaremos por $\pi_0$, $\pi_1$, y $\pi_2$ a las proyecciones a los planos $\Pi_0$, $\Pi_1$, y $\Pi_2$ respectivamente.



\begin{figure}[hbtp]
\centering
\begin{subfigure}[b]{0.45\textwidth}
  \centering
  \def\svgwidth{\linewidth}
  \input{ejem1.pdf_tex}
  \caption{Proyección de $\cl$ en el plano $x=0$.}
  \label{ejem1}
\end{subfigure}%
\qquad
\begin{subfigure}[b]{0.45\textwidth}
  \centering
  \def\svgwidth{\linewidth}
  \input{ejem2.pdf_tex}
  \caption{Proyección de $\cl$ en el plano $x=y$.}
  \label{ejem2}
\end{subfigure}

\centering
\begin{subfigure}[b]{0.45\textwidth}
  \centering
  \def\svgwidth{\linewidth}
  \input{ejem3.pdf_tex}
  \caption{Proyección de $\bcl$ en el plano $x=0$.}
  \label{ejem3}
\end{subfigure}%
\qquad
\begin{subfigure}[b]{0.45\textwidth}
  \centering
  \def\svgwidth{\linewidth}
  \input{ejem4.pdf_tex}
  \caption{Proyección de $\fcl$ en el plano $y=0$.}
  \label{ejem4}
\end{subfigure}
\caption{}
\end{figure}


\clearpage

\subsubsection{Familia del tetraedro}

Para esta sección consideraremos al tetraedro $\cT$ cuyos vértices son $$\{ (1,1,1), (-1,-1,1), (1,-1,-1), (-1,1,-1)\}$$ y su grupo de simetrías es $\Tetra$ descrito en la Sección \ref{real:tetra}. Así $\r_0$ es la reflexión con respecto al plano $\Pi_0=\{(x,y,z) \in \E : x+y=0\}$, $\r_1$ es la reflexión con respecto al plano $\Pi_1=\{(x,y,z) \in \E : x-z=0\}$ y $\r_2$ es la reflexión con respecto al plano $\Pi_2=\{(x,y,z) \in \E: x-y=0 \}$ (Fig. \ref{tetra3}).


%\begin{figure}[hbt]
%\begin{center}
%\begin{scriptsize}
%\def\svgwidth{8cm}
%\input{tetra.pdf_tex}
%\caption{\normalsize Tetraedro y sus planos de reflexión}
%\label{fig:tetracap3}
%\end{scriptsize}
%\end{center}
%\end{figure}

En términos de matrices con respecto a la base canónica $\sC=\{e_1,e_2,e_3\}$:

\begin{eqnarray}
\left[\r_0\right]_{\sC} & = & %
\left( \begin{array}{ccc}%
0 & -1 & 0 \\%
-1 & 0 & 0 \\%
0 & 0 & 1%
\end{array} \right) \label{genteta1} \\ 
%
\left[\r_1\right]_{\sC} & = & %
\left( \begin{array}{ccc} %
0 & 0 & 1 \\ %
0 & 1 & 0 \\ %
1 & 0 & 0 %
\end{array} \right)  \label{genteta2} \\
%
\left[\r_2 \right]_{\sC} & = &%
\left( \begin{array}{ccc}%
0 & 1 & 0 \\%
1 & 0 & 0 \\%
0 & 0 & 1%
\end{array} \right)  \label{genteta3}
\end{eqnarray}

Una vez que hemos descrito el tetraedro con el que trabajaremos procedamos a hacer el análisis de las retículas que quedan invariantes bajo $\Tetra$.

\begin{lema}\label{tetraedrito}
Sean $\r_0$, $\r_1$ y $\r_2$ las reflexiones generadoras de $\Tetra$ descritas arriba, y $S=\r_2 \r_1$ una rotación del tetraedro $\cT$. Si $v_2$ es un vector ortogonal al plano $\Pi_2$, el plano de reflexión de $\r_2$, entonces los puntos $0,v_2, S(v_2)$ y $S^{2}(v_2)$ forman los vértices de un tetraedro regular $\cT'$.

\begin{proof}
Es claro que $e(0,v_2)=e(0,S(v_2))=e(0,S^{2}(v_2))$ así que basta probar que $e(0,v_2)=e(v,S(v_2))$, el resto se sigue de que $S$ es isometría.

Observemos que $\V{v_2}$ es paralelo a la arista $a$ entre los puntos $(1,-1,-1)$ y $(-1,1,-1)$, pues ambos son ortogonales a $\Pi_2$. Esto implica que $S(\V{v_2})$ es paralelo a la arista $S(a)=\{(-1,-1,1),(-1,1,-1)\}$. Tenemos entonces que el ángulo entre $\V{v_2}$ y $S(\V{v_2})$ es $\frac{\pi}{3}$, de modo que los puntos $0$,  $v_2$ y $S(v_2)$ forman un triángulo isóceles con un ángulo de $\frac{\pi}{3}$, y por lo tanto dicho triángulo es equilátero. De aquí se sigue que $e(0,v_2)=e(v_2,S(v_2))$ (Fig \ref{fig:tetraedrito}).
\end{proof}
\end{lema}

\begin{figure}[hbt]
\begin{center}
\begin{scriptsize}
\def\svgwidth{8cm}
\input{tetraedrito.pdf_tex}
\end{scriptsize}
\caption{}
\label{fig:tetraedrito}
\end{center}
\end{figure}

\begin{obs}
El lema anterior es válido también si consideramos $S=\r_2 \r_1$ y el plano $\Pi_0$, o bien $S=\r_2 \r_1 \r_0 \r_2$ y el plano $\Pi_1$, así como respectivos vectores ortogonales.
\end{obs}

\begin{lema} \label{lema:tetra}
Sean $\r_0$, $\r_1$ y $\r_2$ las reflexiones generadoras de $\Tetra$ descritas arriba, $\Pi_i$ el plano de reflexión de $\r_i$ y $\pi_i$ la proyección al plano $\Pi_i$ para $i \in \{ 0,1,2 \}$. Si $\L$ es una retícula que queda invariante bajo $\Tetra$ entonces $\pi_{i}(\L) \neq \Pi_{i} \cap \L$ para $i \in \{0,1,2\}$.
\begin{proof}
Probemos el resultado para $i=2$, los otros casos son análogos. Supongamos que $\pi_2(\L)=\Pi_2 \cap \L$. Por la Proposición \ref{prop:IntYProyCoinciden} podemos tomar vectores $\v_1, \v_2 \in \Pi_2$ y $\v_3 \in \Pi^{\perp}_{2}$ de tal forma que $\L = \langle \v_1, \v_2, \v_3 \rangle$ y la distancia de $\upsilon_3$ a $\Pi_2$ es mínima.

Ya que $\L$ se preserva bajo $\Tetra$ tenemos en particular que $p = \r_1 \r_0 (\v_3) \in  \L$ sin embargo $p$ es un punto entre los planos $\Pi_2$ y $\Pi_2 + \v_3$ (por el Lema \ref{tetraedrito}) lo cual contradice la minimalidad de la distancia de de $\upsilon_3$ a $\Pi_2$.

Para los casos $i=0$ e $i=1$ basta considerar las rotaciones $\r_2 \r_1$ y $\r_2 \r_1 \r_0 \r_2$ respectivamente.
\end{proof}
\end{lema}


El Lema \ref{lema:tetra} nos dice que en las proyecciones de $\L$ en los planos $\Pi_0$ y $\Pi_2$ deben de aparecer puntos ($\star$) y sabemos que éstos deben ser puntos medios de segmentos entre puntos ($\bullet$). Además, como $\L$ es invariante bajo $\Tetra$ las proyecciones en ambos planos deben ser esencialmente iguales pues la reflexión rotatoria $\r_1 \r_2 \r_1 \r_0 \r_1$ intercambia los planos $\Pi_0$ y $\Pi_2$. Bajo estas condiciones hay que analizar las distintas situaciones, considerando primero la distribución de los puntos ($\bullet$), y fijos éstos, determinando las posibilidades para los puntos ($\star$):

\begin{itemize}
\item \textit{Situación 1}: La retícula $\L \cap \Pi_0$ es de traslación paralela con respecto a la recta $\Pi_0 \cap \Pi_2$. Las posiblidades para los puntos ($\star$) son las de las figuras \ref{fig:lt1}, \ref{fig:lt2} y \ref{fig:lt3}. Sin embargo, la figura \ref{fig:lt3} queda descartada por el lema \ref{lema:tetra}, pues $\pi_2(\L)$ consistiría sólo de puntos ($\bullet$).
\item \textit{Situación 2}: La retícula $\L \cap \Pi_0$ no es de traslación paralela respecto a la recta $\Pi_0 \cap \Pi_2$. Notemos que ya que $\L$ se queda invariante bajo $\r_2$ entonces la reflexión por la recta $\Pi_0 \cap \Pi_2$ debe ser una simetría de $\pi_0(\L)$ que respeta además el tipo de punto ($\bullet$ ó $\star$). Es fácil ver que bajo estas condiciones la única posibilidad es la de la Figura \ref{fig:lt4}.
\end{itemize}

Así que los tres casos posibles son: 

\begin{description}
\item[\textbf{Caso 1:}] La retícula se ve como en la figura \ref{fig:lt1}.
\item[\textbf{Caso 2:}] La retícula se ve como en la figura \ref{fig:lt2}.
\item[\textbf{Caso 3:}] La retícula se ve como en la figura \ref{fig:lt4}.
\end{description}



\begin{figure}[hbtp]
\centering
\begin{subfigure}[b]{0.35\textwidth}
  \centering
  \def\svgwidth{\linewidth}
  \input{lattetra1.pdf_tex}
  \caption{}
  \label{fig:lt1}
\end{subfigure}%
\qquad \qquad
\begin{subfigure}[b]{0.35\textwidth}
  \centering
  \def\svgwidth{\linewidth}
  \input{lattetra2.pdf_tex}
  \caption{}
  \label{fig:lt2}
\end{subfigure}

\medskip

\centering
\begin{subfigure}[b]{0.35\textwidth}
  \centering
  \def\svgwidth{\linewidth}
 \input{lattetra3.pdf_tex}
  \caption{}
  \label{fig:lt3}
\end{subfigure}%
\qquad \qquad
\begin{subfigure}[b]{0.35\textwidth}
  \centering
  \def\svgwidth{\linewidth}
  \input{lattetra4.pdf_tex}
  \caption{}
  \label{fig:lt4}
\end{subfigure}
\caption{}
\end{figure}
\clearpage

Dado que las retículas deben ser invariantes bajo la reflexión respecto a $\Pi_0$, por el Lema \ref{lema:capas}, $\L$ puede ser partido en niveles $\L=\bigcup_{k \in \bZ} \L_k$ de tal forma que $\L_k$ es un trasladado de $\Pi_0 \cap \L$. Nuestro análisis consistirá en encontrar dos vectores linealmente independientes $\v_1, \v_2 \in \Pi_0$ y un vector $\v_3 \in \L_1$, pues por los resultados de la sección anterior $\L = \langle \v_1, \v_2, \v_3 \rangle$.

\paragraph*{Caso 1.}
Comenzaremos el análisis para cuando la retícula se ve como en la Figura \ref{fig:lt1}. En esta situación podemos tomar $\v_1$ ortogonal a $\Pi_2$ y $\v_2$ en $\Pi_0 \cap \Pi_2$ (Fig. \ref{fig:lt11}). Gracias a la Observación \ref{obs:lineal} podemos suponer que $\v_1=(1,-1,0)$, además $\v_2=(0,0,a)$ para alguna $a > 0$.

\begin{figure}[hbt]
\begin{center}
\begin{scriptsize}
\def\svgwidth{5cm}
\input{lattetra11.pdf_tex}
\caption{}
\label{fig:lt11}
\end{scriptsize}
\end{center}
\end{figure}

Sea $\v_3$ el punto de $\L_1$ que se proyecta en el punto de $\Pi_0$ con coordenadas $(\frac{1}{2}, -\frac{1}{2},0)$ marcado con ($\star$). Ya que la reflexión rotatoria $\r_1 \r_2 \r_1 \r_0 \r_1$ con eje $\Pi_0 \cap \Pi_1$ centrada en $(0,0,0)$ intercambia los planos dejando la retícula invariante, $\pi_2(\v_3)$ debe estar marcado con ($\star$) en $\Pi_2$, y ya que se eligió en $\L_1$, $\pi_2(\v_3)=(\frac{1}{2}, \frac{1}{2},0)$, pues es el punto medio entre $0$ y $(1,1,0)=\r_1 \r_2 \r_1 \r_0 \r_1((1,-1,0))$. Tenemos entonces que $\v_3 = \pi_0(\v_3)+\pi_2(\v_3)= (1,0,0)$, y además, $\upsilon_3$ es un punto de la retícula en el eje $x$ de distancia mínima positiva al origen. 

Observemos que la rotación $\r_2 \r_1$ manda el rayo $x>0$ en el rayo $z>0$, así que debe de mandar $\v_3$ en $\v_2$ pues $\v_2$ es el punto de la retícula en el rayo $z>0$ más cercano al origen. Tenemos entonces que $\v_2=(0,0,1)$.

Concluimos que la retícula $\L$ debe ser la generada por $(1,-1,0)$, $(0,0,1)$ y $(1,0,0)$. Observemos que $\v_1=(1,-1,0)=(1,0,0)-(0,1,0)$ así que $(0,1,0) = \v_3 - \v_1$ por lo tanto $$\L = \langle (1,0,0), (0,1,0), (0,0,1) \rangle,$$ es decir, $\L=\cl$.

\paragraph*{Caso 2.} Asumamos ahora que la retícula $\Pi_0 \cap \L$ se ve como en la Figura \ref{fig:lt2}. Tomemos de nuevo los vectores $\v_1$ ortogonal a $\Pi_2$ y $\v_2$ en $\Pi_0 \cap \Pi_2$. Gracias a la Observación \ref{obs:lineal} podemos suponer que $\v_1=(1,-1,0)$ y como $\Pi_0 \cap \Pi_2$ es el eje $z$ tenemos que $\v_2=(0,0,a)$ con $a>0$ (Fig. \ref{fig:lt21}).

\begin{figure}[hbt]
\begin{center}
\begin{scriptsize}
\def\svgwidth{5cm}
\input{lattetra21.pdf_tex}
\caption{}
\label{fig:lt21}
\end{scriptsize}
\end{center}
\end{figure}

Consideremos de nuevo la reflexión rotatoria $\r_1 \r_2 \r_1 \r_0 \r_1$ de eje $\Pi_0 \cap \Pi_2$. Ésta intercambia los planos $\Pi_0$ y $\Pi_2$, y entonces $\r_1 \r_2 \r_1 \r_0 \r_1(\v_1)=(1,1,0) \in \L$, y por lo tanto $\v_1+\r_1 \r_2 \r_1 \r_0 \r_1(\v_1)=(2,0,0) \in \L$. Observemos que este es el punto de $\L$ en el rayo $x>0$ más cercano al origen, pues de lo contrario, habría un punto ($\bullet$) o ($\star$) de $\pi_0(\L)$ entre $0$ y $\upsilon_1$. De manera análoga al caso anterior, la rotación $\r_2 \r_1$ debe mandar a $(2,0,0)$ en $\v_2$ por lo tanto $\v_2=(0,0,2)$.

Gracias al Lema \ref{tetraedrito}, $\r_1 \r_0 (\v_1) \in \L_1$, de modo que podemos elegir $\v_3=\r_1 \r_0(\v_1)=(0,-1,1)$ y tenemos que $$\L = \langle \v_1, \v_2, \v_3 \rangle=\langle (1,-1,0),(0,0,2),(0,-1,1) \rangle.$$

Es fácil ver que los vectores $$\v'_1=(1,1,0)=\v_1 + \v_2 - 2 \v_3,$$ $$\v'_2=(1,0,1)=\v_1+\v_2-\v_3 \text{ y } $$  $$\v'_3=(0,1,1)=\v_2-\v_3$$ son generadores de $\L$ y por lo tanto $\L=\fcl$.

\paragraph*{Caso 3.} Supongamos ahora que la retícula $\L \cap \Pi_0$ no es de traslación paralela respecto a la recta $\Pi_0 \cap \Pi_2$. Podemos tomar $\v_1 \in \Pi_0$ ortogonal a $\Pi_0 \cap \Pi_2$ y $\V{u} \in \Pi_0 \cap \Pi_2$ de modo que $\L \cap \Pi_0 = \langle \v_1, \v_2 \rangle$ con $\v_2=\frac{1}{2}\v_1+\frac{1}{2} \V{u}$ (Fig. \ref{fig:lt31}). Gracias a la Observación \ref{obs:lineal} podemos suponer que $\v_1=(2,-2,0)$, además $\V{u}=(0,0,a)$ con $a>0$. 

\begin{figure}[hbt]
\begin{center}
\begin{scriptsize}
\def\svgwidth{5cm}
\input{lattetra31.pdf_tex}
\caption{}
\label{fig:lt31}
\end{scriptsize}
\end{center}
\end{figure}

De manera análoga al Caso 1, si sumamos el punto $(1,-1,0) \in \pi_0(\L)$ con su imagen $(1,1,0)$ bajo la reflexión rotatoria $\r_1 \r_2 \r_1 \r_0 \r_1$, tenemos que el punto $(2,0,0)$ es el punto de $\L$ en el rayo $x>0$ más cercano al origen, y usando la rotación $\r_2 \r_1$ concluímos que $a=2$ y $\V{u}=(0,0,2)$. Esto implica que $\v_2=(1,-1,1)$.

Observemos además que el punto $\v_3=(2,0,0)$ pertenece a $\L_1$ y puede ser elegido como tercer generador, de modo que $$\L=\langle \langle \v_1, \v_2, \v_3 \rangle=\langle (2,-2,0),(1,-1,1),(2,0,0) \rangle.$$

Podemos tomar como generadores a $\v'_1=(0,2,0)=\v_3-\v_1$, $\v'_2=(1,1,1)=\v_3+\v_2-\v_1$ y $\v_3=(2,0,0)$ y entonces es claro que $\L=\bcl$.

Gracias al análisis hecho hemos reducido las retículas que se preservan bajo $\Tetra$ a unos pocos casos. El siguiente teorema clasifica de manera completa las retículas que se preservan bajo $\Tetra$.

\begin{teo} \label{teo:tetra}
Sea $\T$ un grupo generado por tres traslaciones linealmente independientes. El tetraedro regular $\cT$ admite realización en $\Tt$ si y sólo si $$\L_{\T} \in \{a\cl, b\fcl, c\bcl: a > 2, b > 2, c > \frac{2}{\sqrt{3}} \}.$$
\begin{proof}
Por la Observación \ref{obs:lineal}, basta probar que $\cl$, $\fcl$ y $\bcl$ sí son invariantes bajo $\Tetra$.

Es claro que $\cl$ es invariante bajo $\r_0$, $\r_1$ y $\r_2$ pues las matrices $[\r_0]_{\sC}$, $[\r_1]_{\sC}$ y $[\r_2]_{\sC}$ tienen entradas enteras.

Las matrices $[\r_1]_{\sC}$ y $[\r_2]_{\sC}$ son matrices de permutación, de modo que la suma de coordenadas de todo vector $v \in \E$ es igual a la suma de coordenadas de $\r_i(v)$ para $i \in \{1,2\}$. La matriz $[\r_0]_{\sC}$ cambia de signo y permuta la primera y la segunda coordenada de un vector, lo que preserva la paridad de la suma de sus coordenadas. Tenemos entonces que $\fcl$ es invariante bajo $\Tetra$.

Con la descripción anterior es claro que $[\r_0]_{\sC}$, $[\r_1]_{\sC}$ y $[\r_2]_{\sC}$ preservan al conjunto de vectores de $\bZ^3$ cuyas cordenadas tienen la misma paridad, así que $\bcl$ queda invariante bajo $\Tetra$.

El hecho de que $a>2$, $b>2$ y $c>\frac{2}{\sqrt{3}}$ viene de las restricciones combinatorias descritas en la Nota \ref{notodas}. Pues si $a=2$ o $b=2$ entonces todos los vértices serían uno sólo en $\Tt$, para el grupo $\T$ correspondiente; si $c = \frac{2}{\sqrt{3}}$ cada vértice sería identificado con el centro de la cara opuesta.
\end{proof}
\end{teo}

El teorema \ref{teo:tetra} se extiende de manera natura a la familia del tetraedro, pues el grupo de simetrías coincide para todos los poliedros de dicha familia.

\begin{coro}
Sea $\T$ un grupo de generado por tres traslaciones linealmente independientes. El poliedro $\{4,3\}_3$ admite realización en $\Tt$ si y sólo si $$\L_{\T} \in \{a\cl, b\fcl, c\bcl: a > 2, b>2, c>\frac{2}{\sqrt{3}} \}.$$
\end{coro}

\FloatBarrier
\subsubsection{Familia del octaedro}

Como representante de esta familia consideraremos el octaedro $\cO$ de vértices $\{(\pm 1, 0, 0),(0, \pm 1, 0),(0,0,\pm 1) \}$ y su grupo de simetrías $\Octa$ descrito en la sección \ref{real:octa}. Tenemos entonces que $\r_0$ es la reflexión con respecto al plano $\Pi_0 = \{ (x,y,z) \in \E : x-z=0 \}$, $\r_1$ es la reflexión respecto al plano $\Pi_1 = \{ (x,y,z) \in \E : x-y=0 \}$ y $\r_2$ es la reflexión respecto al plano $\Pi_2 = \{ (x,y,z) \in \E : y=0 \}$ (Fig \ref{octa3}).

%\begin{figure}[hbt]
%\begin{center}
%\begin{scriptsize}
%\def\svgwidth{8cm}
%\input{octa.pdf_tex}
%\caption{\normalsize Octaedro y sus planos de reflexión.}
%\label{fig:octacap3}
%\end{scriptsize}
%\end{center}
%\end{figure}

Cuyas matrices con respecto a la base canónica $\sC$ son:

\begin{eqnarray} 
\left[\r_0\right]_{\sC} & = & %
\left( \begin{array}{ccc}%
0 & 0 & 1 \\%
0 & 1 & 0 \\%
1 & 0 & 0%
\end{array} \right)  \label{genocta1} \\
%
\left[\r_1\right]_{\sC} & = & %
\left( \begin{array}{ccc} %
0 & 1 & 0 \\ %
1 & 0 & 0 \\ %
0 & 0 & 1 %
\end{array} \right)  \label{genocta2} \\
%
\left[\r_2 \right]_{\sC} & = &%
\left( \begin{array}{ccc}%
1 & 0 & 0 \\%
0 & -1 & 0 \\%
0 & 0 & 1%
\end{array} \right) \label{genocta3}
\end{eqnarray}

La siguiente proposición nos da una relación entre los grupos $\Tetra$ y $\Octa$ que simplificará el trabajo.

\begin{prop}\label{prop:TmetidoenO}
Sean $\cT$ el tetraedro y $\cO$ el octaedro descritos anteriormente, entonces $\Tetra \leq \Octa$.
\begin{proof}
Sean $\sigma_0$, $\sigma_1$ y $\sigma_2$ los generadores de  $\Tetra$ de (\ref{genteta1}), (\ref{genteta2}) y (\ref{genteta3}) respectivamente; sean $\r_0$, $\r_1$ y $\r_2$ los generadores de  $\Octa$ de (\ref{genocta1}), (\ref{genocta2}) y (\ref{genocta3}). Tenemos que $\s_1=\r_0$ y $\s_2=\r_1$, así que basta observar que $\s_0=\r_1 \r_2 \r_1 \r_2 \r_1$. 
\end{proof}
\end{prop}

Notemos que, gracias a la Proposición \ref{prop:TmetidoenO}, toda retícula que sea preservada por $\Octa$ va a ser preservada por $\Tetra$ así que resta verificar si las retículas $\cl$, $\fcl$ y $\bcl$ son preservadas por $\Octa$.

\begin{teo} \label{teo:octa}
Sea $\T$ un grupo de generado por tres traslaciones linealmente independientes. El octaedro regular admite realización en $\Tt$ si y sólo si $$\L_{\T} \in \{a\cl, b\fcl, c\bcl: a > 2, b>1 c>1 \}.$$
\begin{proof}
Por la Observación \ref{obs:lineal}, basta probar que las retículas $\cl$, $\fcl$ y $\bcl$ quedan invariantes bajo $\Octa$.

Es claro que $\cl$ es invariante bajo $\r_0$, $\r_1$ y $\r_2$ pues $[\r_0]_{\sC}$, $[\r_1]_{\sC}$ y $[\r_2]_{\sC}$ tienen entradas enteras.

Notemos que $\r_1$ y $\r_2$ son simetrías del tetraedro $\cT$, así que preservan a $\cl$, $\fcl$ y $\bcl$. La matriz $[\r_0]_{\sC}$ es una matriz de permutación, de modo que la suma de coordenadas de todo vector $v \in \E$ es igual a la suma de coordenadas de $\r_0(v)$, de modo que $\r_0$ preserva a $\fcl$. Es claro que $\r_0$ preserva a $\cl$ y $\bcl$. Por lo tanto, las tres retículas se preservan bajo $\Octa$.
 
De manera similar al teorema \ref{teo:tetra}, el hecho de que $a>2$, $b>1$ y $c>1$ viene de las restricciones combinatorias descritas en la Nota \ref{notodas}.
\end{proof}
\end{teo}

\begin{coro}\label{coro:cubo}
Sea $\T$ un grupo de generado por tres traslaciones linealmente independientes. El cubo $\cC$ admite realización en $\Tt$ si y sólo si $$\L_{\T} \in \{a\cl, b\fcl, c\bcl: a > 2, b>2, c>2 \}.$$
\begin{proof}
Se sigue del hecho de que $\G(\cC)=\Octa$. El ajuste de los parámetros $a$, $b$ y $c$ se debe a las coordenadas de los vértices de $\cC$. 
\end{proof}
\end{coro}

Se pueden obtener resultados similares al corolario \ref{coro:cubo} para cada uno de los poliedros de la Familia del Octaedro, sin embargo, de la misma manera a cómo se hizo para el cubo, los parámetros $a$, $b$, $c$ deben ser ajustados para cada uno de los poliedros de acuerdo a las coordenadas de sus vértices.

\FloatBarrier
\subsubsection{Familia del icosaedro}

El siguiente reultado sirve para determinar por completo las retículas que se quedan invariantes bajo el grupo de simetrías del icosaedro $\Ico$. Una prueba de éste se puede encontrar en \cite[Teo. 4.22]{yale}.

%\begin{lema}\label{lema:nohaydea5}
%Si $S$ es una rotación en $\E$ de ángulo $\theta$ respecto a un eje que pasa por el origen, entonces existe una base ortonormal $\cB$ de $\E$ de tal forma que 
%$$
%\left[ S \right]_{\cB}  = %
%\left( \begin{array}{ccc}%
%\cos \theta & \sin \theta & 0 \\%
%-\sin \theta & \cos \theta & 0 \\%
%0 & 0 & 1%
%\end{array} \right)
%$$
%\end{lema}

\begin{teo}\label{prop:nohaydea5}
Sea $\T$ un grupo generado por $3$ traslaciones linealmente independientes. Si $G$ es un grupo de isometrías de $\E$ que deja invariante a $\L_{\T}$, entonces $G$ no tiene rotaciones de orden distinto a $2$, $3$, $4$ o $6$.
\end{teo}

Ahora estamos listos para determinar las realizaciones de la familia del icosaedro en $\TT$.

\begin{teo}\label{teo:ico}
Si $\P$ es un poliedro de la familia del icosaedro, entonces no existe $\T$, un grupo generado por $3$ traslaciones linealmente independientes, de tal forma que $\P$ tenga realización en $\Tt$.
\begin{proof}
Sea $\T$ un grupo generado por $3$ traslaciones linealmente independientes. Sin pérdida de generalidad $\P$ es el icosaedro descrito en la Sección \ref{real:ico},  $\Gamma(\P) = \Ico = \langle \r_0, \r_1, \r_2 \rangle$. Entonces $\r_1 \r_2$ es una rotación de orden $5$ y por el Teorema \ref{prop:nohaydea5} $ \Ico $ no deja invariante a $\L_{\T}$, así que $\P$ no tiene realización en $\Tt$.
\end{proof}
\end{teo}


\section{Poliedros de Petrie-Coxeter}\label{polPCT3}

En esta sección trabajaremos con los poliedros de Petrie-Coxeter descritos en la Sección \ref{real:PC}. Usaremos los resultados obtenidos en la sección anterior para determinar las retículas invariantes bajo sus grupos de simetrías.

Los resultados de esta sección se basan en dos ideas principales:
\begin{itemize}
\item Relaciones entre los grupos de simetrías de los poliedros de Petrie-Coxeter y los grupos de simetría de los poliedros finitos.
\item En general, las simetrías de los poliedros de esta familia no son lineales, pero gracias al Corolario \ref{coro:bastaprima}, bastará probar que la parte lineal de ellas preserva a las retículas.
\end{itemize}


\begin{nota}\label{notodastampoco}
Dado que estos poliedros son infinitos y que $\Tt$ es compacto para todo grupo $\T$, es imposible evitar que en el cociente se identifiquen vértices, aristas o caras, sin crear puntos de acumulación en los vértices. Por esta razón, consideraremos únicamente aquellos grupos $\T$ que satisfagan que $\T \leq T$ donde $T$ es el subgrupo de traslaciones del grupo de simetrías del poliedro. De esta forma, los vértices, aristas y caras serán identificados preservando, al menos de manera local, la combinatoria.
\end{nota}

\FloatBarrier
\subsubsection{Los Poliedros $\{4,6|4\}$ y $\{6,4|4\}$}

Como representante de esta familia consideraremos el poliedro $\PCa$ descrito en la Sección \ref{sec:RPR} (Fig. \ref{fig:PC1cap3}).

\begin{figure}[hbt]
\begin{center}
\begin{scriptsize}
\def\svgwidth{8cm}
\input{PC1.pdf_tex}
\caption{\normalsize $\PCa$}
\label{fig:PC1cap3}
\end{scriptsize}
\end{center}
\end{figure} 

El grupo $\G(\PCa)$ es generado por $\r_0$ la reflexión con respecto al plano $\Pi_0 = \{ (x,y,z) \in \E : y=0 \}$, $\r_1$ el medio giro respecto a la recta $\Pi_1$, determinada por los puntos $(1,1,1)$ y $(-3,-1,1)$, y $\r_2$ la reflexión con respecto al plano $\Pi_2 = \{ (x,y,z) \in \E : x-z=0 \}$.

Calculando las matrices de $\r'_0$, $\r'_1$ y $\r'_2$ con respecto a la base canónica $\sC$ tenemos:

\begin{eqnarray*}
\left[\r'_0\right]_{\sC} & = & %
\left( \begin{array}{ccc}%
1 & 0 & 0 \\%
0 & -1 & 0 \\%
0 & 0 & 1%
\end{array} \right) \\
%
\left[\r'_1\right]_{\sC} & = & %
\left( \begin{array}{ccc} %
0 & -1 & 0 \\ %
-1 & 0 & 0 \\ %
0 & 0 & -1 %
\end{array} \right) \\
%
\left[\r'_2 \right]_{\sC} & = &%
\left( \begin{array}{ccc}%
0 & 0 & 1 \\%
0 & 1 & 0 \\%
1 & 0 & 0%
\end{array} \right) \\
\end{eqnarray*}

\begin{prop} \label{OmetidoenPC1}
Sean $\cO$ octaedro regular y $\PCa$ el poliedro de Petrie-Coxeter descritos en la sección \ref{sec:RPR}, entonces $\Octa \leq \G(\PCa)$.
\begin{proof}
Sean $\s_0, \s_1, \s_2$ los generadores distinguidos de $\Octa$  y $\r_0, \r_1, \r_2$ los generadores distinguidos de $\G(\PCa)$. Tenemos que $\s_0=\r_2$ y $\s_2=\r_0$, así que basta observar que $\s_1=\r_2 \r_1 \r_2 \r_1 \r_2$ y por lo tanto $\Octa \leq \G(\PCa)$.
\end{proof}
\end{prop}

La Proposición anterior nos da una restricción para las retículas preservadas por $\G(\PCa)$, pues todas ellas deben ser preservadas por $\Octa$, el siguiente Teorema nos da la clasificación completa de éstas.

\begin{teo} \label{teo:PC1}
Sea $\T$ un grupo de generado por tres traslaciones linealmente independientes. El poliedro $\PCa$ y admite realización en $\Tt$ si y sólo si $$\L_{\T} \in \{4a\cl, 4b\fcl, 2c\bcl: a,b,c \in \bZ \}.$$

\begin{proof}
Gracias a la Proposición \ref{OmetidoenPC1} y a la Observación \ref{obs:lineal} basta ver que las retículas $\cl$, $\fcl$ y $\bcl$ se preservan bajo $\r'_0$, $\r'_1$ y $\r'_2$.

Por el análisis hecho en la prueba del Teorema \ref{teo:octa} tenemos que $\r'_0$ y $\r'_2$ preservan a las retículas $\cl$, $\fcl$ y $\bcl$. Observemos que $-[\r'_1]_{\sC}$ es una matriz de permutación, por lo tanto preserva coordenadas enteras, paridad de las coordenadas de un vector y paridad de la suma de coordenadas, así que preserva a las tres retículas. La matriz $-Id$ preserva cualquier retícula, por lo tanto $[\r'_1]_{\sC}$ preserva a $\cl$, $\fcl$ y $\bcl$.

El hecho de considerar únicamente múltiplos de $4$ para $\cl$ y $\fcl$ y múltiplos de $2$ para $\bcl$ viene del requerimiento de que $\T$ sea subgrupo del grupo de traslaciones de $\G(\PCa)$.
\end{proof}
\end{teo}

\begin{coro}
Si $\P$ es un poliedro de la familia de $\{4,6|4\}$ entonces $\P$ admite realización en $\Tt$ si y sólo si $$\L_{\T} \in \{4a\cl, 4b\fcl, 2c\bcl: a,b,c \in \bZ \}.$$
\end{coro}

\FloatBarrier
\subsubsection{El poliedro $\{6,6|3\}$ }

Consideraremos el poliedro $\PCc$ descrito en la sección \ref{real:PC3} (Fig. \ref{fig:PC3cap3}).

\begin{figure}[hbt]
\begin{center}
\begin{scriptsize}
\def\svgwidth{8cm}
\input{PC3.pdf_tex}
\caption{\normalsize $\PCc$}
\label{fig:PC3cap3}
\end{scriptsize}
\end{center}
\end{figure} 

Así, $\G(\PCc)$ es el grupo generado por $\r_0$, la reflexión con respecto al plano $\Pi_0 = \{ (x,y,z) \in \E : x+y=0 \}$, $\r_1$ el medio giro respecto a la recta $\Pi_1=\{ (x,y,z) \in \E : x=1, \ z+y=2 \}$ y $\r_2$ la reflexión con respecto al plano $\Pi_2 = \{ (x,y,z) \in \E : x-y=0 \}$.

Calculando las matrices de $\r'_0$, $\r'_1$ y $\r'_2$ con respecto a la base canónica $\sC$ tenemos:

\begin{eqnarray*}
\left[\r'_0\right]_{\sC} & = & %
\left( \begin{array}{ccc}%
0 & -1 & 0 \\%
-1 & 0 & 0 \\%
0 & 0 & 1%
\end{array} \right) \\
%
\left[\r'_1\right]_{\sC} & = & %
\left( \begin{array}{ccc} %
-1 & 0 & 0 \\ %
0 & 0 & -1 \\ %
0 & -1 & 0 %
\end{array} \right) \\
%
\left[\r'_2 \right]_{\sC} & = &%
\left( \begin{array}{ccc}%
0 & 1 & 0 \\%
1 & 0 & 0 \\%
0 & 0 & 1%
\end{array} \right) \\
\end{eqnarray*}

\begin{prop} \label{TmetidoenPC3}
Sean $\Tetra$ el grupo de simetrías del tetraedro y $\G(\PCc)$ y el grupo de simetrías de $\PCc$, entonces $\Tetra \leq \G(\PCa)$.
\begin{proof}
Sean $\s_0, \s_1, \s_2$ los generadores distinguidos de $\Tetra$ y $\r_0, \r_1, \r_2$ los generadores distinguidos de $\G(\PCc)$. Tenemos que $\s_0=\r_0$ y $\s_2=\r_2$, así que basta observar que $\s_1=\r_2 \r_1 \r_2 \r_1 \r_2$ y por lo tanto $\Tetra \leq \G(\PCc)$.
\end{proof}
\end{prop}

De manera análoga al Poliedro $\PCa$, el hecho de que $\Tetra \leq \G(\PCc)$ limita las posibilidades para los grupos $\T$. Con el siguiente teorema terminamos la clasificación de tales grupos para $\PCc$.

\begin{teo} \label{teo:PC3}
Sea $\T$ un grupo de generado por tres traslaciones linealmente independientes. El poliedro $\PCc$ admite realización en $\Tt$ si y sólo si $$\L_{\T} \in \{8a\cl, 4b\fcl, 8c\bcl: a,b,c \in \bZ \}.$$

\begin{proof}
Gracias a la Proposición \ref{OmetidoenPC1} y a la Observación \ref{obs:lineal} basta ver que las retículas $\cl$, $\fcl$ y $\bcl$ se preservan bajo $\r'_0$, $\r'_1$ y $\r'_2$.

%Gracias a que las retículas son invariantes bajo $\Tetra$, en particular son invariantes bajo $\r_0$ y $\r_2$. Para ver que $\r_2$ preserva a las retículas basta argumentar de manera análoga a como se hizo para el Teorema \ref{teo:PC1}, pues $-[\r'_1]_{\sC}$ es matriz de permutación.

El hecho de considerar únicamente múltiplos de $8$ para $\cl$, de $4$ para $\fcl$ y de $8$ para $\bcl$ viene del requerimiento de que $\T$ sea subgrupo del grupo de traslaciones de $\G(\PCc)$.
\end{proof}

\begin{coro}
El poliedro $\{6,6|3\}^{\pi}$, petrial de $\{6,6|3\}$, admite realización en $\Tt$ si y sólo si $$\L_{\T} \in \{8a\cl, 4b\fcl, 8c\bcl: a,b,c \in \bZ \}.$$
\end{coro}

\end{teo}