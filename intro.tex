\chapter{Introducción}
Las ideas de simetría y belleza han estado relacionadas a través de la historia. Probablemente ésta sea la razón por la que los \emph{poliedros regulares} han sido estudiados desde la antigüedad, pues poseen un grado de simetría llamativo a simple vista.

Incluso antes de que las ideas de geometría fueran formuladas por los griegos el cubo era bien conocido, pues era usado como dado. Los egipcios también conocían el octaedro y tetraedro, de hecho, una pregunta constante a través de los años ha sido ¿por qué las pirámides de Guiza tienen determinada forma? No sería de sorprenderse que éstas fueran planeadas para ser medios octaedros, de hecho, el ángulo entre sus paredes oscila entre $50^{\circ}47'$ y $54^{\circ}14'$, mientras que el del octaedro es $\arccos(\frac{1}{\sqrt{3}})\approx 54^{\circ} 44'$.

Los \emph{Sólidos Platónicos} fueron nombrados en honor a Platón, quien estudió estos cuerpos no sólo por interés matemático, sino por cierto interés místico, ya que estaban relacionados con los elementos: el tetraedro con el fuego, el octaedro con el agua, el cubo con la tierra y el icosaedro con el aire. Curiosamente, el dodecaedro no aparecía como miembro de esta familia, sin embargo, se le asociaba con la forma del universo. 

Euclides, en \emph{Los Elementos} estudia de manera formal los sólidos platónicos. El libro XIII trata esencialmente de estos objetos, y de hecho, prueba que existen únicamente 5 sólidos convexos regulares, es decir, de caras congruentes acomodadas de la misma manera alrededor de cada vértice. La prueba de Euclides se sigue usando hasta estos días y se basa en el hecho de que el ángulo interno de un $p$-ágono es $(1-\frac{2}{p})\pi$, y si se pretenden poner $q$ de ellos alrededor de cada vértice de manera que el sólido sea convexo se debe satisfacer $\frac{1}{p}+\frac{1}{q} \geq \frac{1}{2}$.

El asunto de los poliedros regulares parecía estar terminado con los sólidos platónicos, sin embargo, en grabados y pinturas de la Edad Media aparecieron algunos objetos con un fuerte grado se simetría. Dos de estos objetos fueron estudiados por Johannes Kepler (1571–1630). Estos objetos comparten propiedades combinatorias con los sólidos platónicos, pero tienen la característica de tener caras estrelladas. 

A principios del siglo XIX Louis Poinsot (1777-1859) redescubrió los sólidos de Kepler y dos sólidos más, cuyas caras son convexas pero están acomodadas de manera estrellada alrededor de cada vértice. En 1811 Augustin Louis Cauchy (1789–1857) probó que los cuatro poliedros regulares estrellados descubiertos por Kepler y Poinsot eran todos los posibles construidos de esta manera. 

A mediados del mismo siglo Ludwig Schläfli (1814-1895) realizó aportaciones a la geometría en dimensiones superiores, entre otras, introdujo el concepto de politopo como generalización de polígono y poliedro en dimensiones superiores y encontró todos los politopos y teselaciones regulares en dimensión 4 o más.

Sin duda alguna las aportaciones de Coxeter (1907-2003) forman un pilar importante en la teoría de poliedros. Muchas de estas aportaciones culminaron en su famoso libro \emph{Regular Polytopes} (\cite{regpol}) cuya primera edición fue publicada en 1948. Sin embargo, una de sus aportaciones más relevantes vino cuando apenas era un estudiante, cuando junto con J. F. Petrie (1907–1972) descubrieron 3 objetos con propiedades de simetría similares a las de los poliedros conocidos hasta ese entonces, pero con la característica de tener una infinidad de caras. Coxeter probó también que la lista de estos tres era completa.

La teoría tomó un nuevo aire cuando en 1975 Branko Grünbaum dio una lista de 47 objetos que tenían propiedades geométricas similares a los poliedros regulares (ver \cite{oldandnew}). Esta lista incluía los sólidos platónicos, los sólidos de Kepler-Poinsot y los de Petrie-Coxeter; además incluía las teselaciones del plano con cuadrados, triángulos y hexágonos. Grünbaum permitió también que las caras no fueran planas e incluso podían ser infinitas.

En 1981 A. Dress encontró otro poliedro (\cite{dress1}) para completar la lista de Grünbaum a 48, y en 1985, usando herramientas algebraicas y combinatorias probó que la lista era completa (ver \cite{dress2}).

Justo entre los dos artículos de Dress, en 1982 Danzer y Schulte introdujeron el concepto de \emph{politopo abstracto} (ver \cite{danzer}), concepto que generaliza a los polígonos y poliedros geométricos, rescatando su estructura combinatoria. Finalmente en \cite{ordinary}, Egon Schulte y Peter McMullen abordan el problema geométrico partiendo del concepto de poliedro abstracto y prueban, de una forma distinta, que la lista de 48 poliedros regulares es completa.

Una vez resuelto el problema en el espacio euclidiano es natural preguntarse qué pasa en otros espacios, en este sentido se tienen algunas respuestas:
\begin{itemize}
\item En \cite[c. 8]{GRfDG} Coxeter y Moser describen las teselaciones regulares en el $2$-toro.
\item En \cite[\S 6D,6E]{ARP} Schulte y McMullen encuentran todas las teselaciones regulares del $n$-toro.
\item En \cite{roli1} y \cite{roli2} Javier Bracho junto con otros autores encuentra algunos poliedros en el espacio proyectivo $\bP^3(\bR)$ y finalmente, en \cite{FDRP} McMullen clasifica los poliedros en $\bS^3$, completando de paso la lista de poliedros en $\bP^3(\bR)$.
\item En \cite{RHHS} Coxeter estudia las teselaciones regulares del espacio hiperbólico.
\end{itemize}


El $3$-toro es una $3$-variedad que surge de manera natural, pues es cociente del espacio euclideano. Además tiene propiedades interesantes, por ejemplo, en \cite[\S 6G]{ARP} Schulte y McMullen prueban que es la única $3$-variedad euclideana que admite teselaciones regulares.

En este trabajo abordamos el problema de encontrar los poliedros regulares en el $3$-toro. Trabajamos con los sólidos platónicos y con los poliedros de Petrie-Coxeter por dos razones, ser los primeros en aparecer históricamente y ser los únicos de la lista de 48 en tener caras finitas y planas con casco afín tridimensional.

En el primer capítulo damos algunos resultados generales de la teoría de poliedros. En el segundo introducimos el $3$-toro y su métrica y estudiamos algunas de sus isometrías. En este capítulo también introducimos las retículas de puntos y estudiamos aquellas que son invariantes bajo reflexiones. Finalmente, en el capítulo 3 atacamos el problema principal del trabajo y determinamos cuándo los sólidos platónicos y los poliedros de Petrie-Coxeter pueden ser vistos como poliedros regulares en el $3$-toro. Esto da 8 familias de poliedros regulares en el $3$-toro, cada una de ellas asociada a uno de los poliedros de $\E$ mencionados. Como resultado de nuestro análisis se obtienen $16$ poliedros regulares más en el $3$-toro, todos provenientes de la lista de $48$ poliedros regulares en $\E$.

